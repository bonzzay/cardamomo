<?php

/*
Apuntes reunión SEO

Url que sean las mismas.
contenido igual
etiquetas

Ernesto del valle


*/
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
 
/**
 * Here, we are connecting '/' (base path) to controller called 'Pages',
 * its action called 'display', and we pass a param to select the view file
 * to use (in this case, /app/View/Pages/home.ctp)...
 */
	Router::connect('/', array('controller' => 'pages', 'action' => 'home'));
/**
 * ...and connect the rest of 'Pages' controller's URLs.
 */
	Router::connect('/pages/*', array('controller' => 'pages', 'action' => 'display'));


	Router::connect('/admin', array('controller' => 'users', 'action' => 'login', 'admin' => true));
	
	Router::connect('/admin/menus/add', array('controller' => 'menus', 'action' => 'add', 'admin' => true));
	Router::connect('/admin/menus/delete', array('controller' => 'menus', 'action' => 'delete', 'admin' => true));
	Router::connect('/admin/menus/edit', array('controller' => 'menus', 'action' => 'edit', 'admin' => true));
	//Router::connect('/admin/menus/:id', array('controller' => 'menus', 'action' => 'index', 'admin' => true));
	Router::connect('/calendar', array('controller' => 'pages', 'action' => 'calendar'));
	Router::connect('/calendar/*', array('controller' => 'pages', 'action' => 'calendar'));
	Router::connect('/images', array('controller' => 'pages', 'action' => 'images'));
	Router::connect('/artists/reorder', array('controller' => 'artists', 'action' => 'reorder'));
	Router::connect('/artists', array('controller' => 'pages', 'action' => 'artists'));
	Router::connect('/artists/*', array('controller' => 'pages', 'action' => 'artist'));
	Router::connect('/feed', array('controller' => 'pages', 'action' => 'feed'));
	Router::connect('/event/*', array('controller' => 'pages', 'action' => 'event'));
	Router::connect('/order', array('controller' => 'pages', 'action' => 'order'));
	Router::connect('/day/*', array('controller' => 'pages', 'action' => 'day'));
	Router::connect('/paypalnotification', array('controller' => 'pages', 'action' => 'paypalnotification'));
	Router::connect('/paypalnotification/*', array('controller' => 'pages', 'action' => 'paypalnotification'));
	Router::connect('/jsonevents', array('controller' => 'pages', 'action' => 'eventshome'));
	/* spanish routes */
	Router::connect(__('/fotos-y-videos-portada'), array('controller' => 'pages', 'action' => 'galleries'));
	Router::connect(__('/galeria-de-fotos'), array('controller' => 'pages', 'action' => 'images'));
	Router::connect(__('/localizacion-contacto'), array('controller' => 'pages', 'action' => 'localizacion'));
	
	

	Router::connect(
	    '/admin/menus/:id', // E.g. /blog/3-CakePHP_Rocks
	    array('controller' => 'menus', 'action' => 'index', 'admin' => true),
	    array(
		        // order matters since this will simply map ":id" to
		        // $articleId in your action
		        //'pass' => array('slugc', 'slug'),
	    		'pass' => array('id'),
		        'id' => '[0-9]+'
		    )
	);

	Router::connect(
	    '/:slug', // E.g. /blog/3-CakePHP_Rocks
	    array('controller' => 'pages', 'action' => 'content'),
	    array(
		        // order matters since this will simply map ":id" to
		        // $articleId in your action
		        'pass' => array('slug'),
		        //'id' => '[0-9]+'
		    )
	);
	

/**
 * Load all plugin routes. See the CakePlugin documentation on
 * how to customize the loading of plugin routes.
 */
	Router::parseExtensions();
	Router::setExtensions(array('json', 'xml', 'rss', 'pdf'));
	CakePlugin::routes();


/**
 * Load the CakePHP default routes. Only remove this if you do not want to use
 * the built-in default routes.
 */
	require CAKE . 'Config' . DS . 'routes.php';
