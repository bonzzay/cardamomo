<?php
/**
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link http://cakephp.org CakePHP(tm) Project
 * @package Cake.Console.Templates.default.views
 * @since CakePHP(tm) v 1.2.0.5234
 * @license MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
?>
<div id="content" class="row">
	<ul class="breadcrumb">
		<li><a href="<?php echo "<?php echo \$this->Html->url('/'); ?>"; ?>" class="glyphicons home"><i></i> Home</a></li>
		<li class="divider"></li>
		<li><?php printf("<?php echo __('%s %s'); ?>", Inflector::humanize($action), $singularHumanName); ?></li>
	</ul>
	<div class="separator bottom"></div>
<!-- // Breadcrumb END -->

	<h3><?php printf("<?php echo __('%s %s'); ?>", Inflector::humanize($action), $singularHumanName); ?></h3>

	
		<div class="innerLR">

		<?php echo "<?php echo \$this->Form->create('{$modelClass}', array('role' => 'form', 'class' => 'form-horizontal')); ?>\n"; ?>
	
		<!--form class="form-horizontal" style="margin-bottom: 0;" id="validateSubmitForm" method="get" autocomplete="off" novalidate="novalidate"-->
		
		<!-- Widget -->
			<div class="widget">
			
				<!-- Widget heading -->
				<div class="widget-head">
					<h4 class="heading"><?php printf("<?php echo __('%s %s'); ?>", Inflector::humanize($action), $singularHumanName); ?></h4>
				</div>
								<!-- // Widget heading END -->
				
								<div class="widget-body row">
									<div class="col-md-12">
										<?php
											foreach ($fields as $field) {
												if (strpos($action, 'add') !== false && $field == $primaryKey) {
													continue;
												} elseif (!in_array($field, array('created', 'modified', 'updated'))) {
													echo "\t\t\t\t\t<div class=\"form-group\">\n";
													echo "\t\t\t\t\t\t<?php echo \$this->Form->input('{$field}', array('class' => 'form-control')); ?>\n";
													echo "\t\t\t\t\t</div><!-- .form-group -->\n";
												}
											}
											if (!empty($associations['hasAndBelongsToMany'])) {
												foreach ($associations['hasAndBelongsToMany'] as $assocName => $assocData) {
													echo "\t\t\t\t\t<div class=\"form-group\">\n";
													echo "\t\t\t\t\t\t\t<?php echo \$this->Form->input('{$assocName}');?>\n";
													echo "\t\t\t\t\t</div><!-- .form-group -->\n";
												}
											}
											echo "\n";
											
										?>
									</div>
										
									<!-- // Row END -->
									
										
								</div>
								<!-- // Row END -->
				
					<hr class="separator">
					
					<!-- Form actions -->
					<div class="form-actions">

						<button type="submit" class="btn btn-icon btn-primary glyphicons circle_ok"><i></i>Save</button>
						
					</div>
					<!-- // Form actions END -->
					
			</div>
		</div>
		<!-- // Widget END -->
		
	<?php echo "\t\t\t<?php echo \$this->Form->end(); ?>\n";?>
			
	</div><!-- /#page-content .col-sm-9 -->