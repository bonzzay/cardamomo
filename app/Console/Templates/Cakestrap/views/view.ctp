<?php
/**
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       Cake.Console.Templates.default.views
 * @since         CakePHP(tm) v 1.2.0.5234
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
?>
<div id="content" class="row">
<ul class="breadcrumb">
	<li><a href="<?php echo "<?php echo \$this->Html->url('/'); ?>"; ?>" class="glyphicons home"><i></i> Home</a></li>
	<li class="divider"></li>
	<li><?php echo "<?php echo __('{$pluralHumanName}'); ?>"; ?></li>
</ul>
<div class="separator bottom"></div>
<!-- // Breadcrumb END -->

<h3><?php echo "<?php  echo __('{$singularHumanName}'); ?>"; ?></h3>
	<div class="innerLR">
		<div class="widget">
		
			<div class="widget-head">
				<h4 class="heading"><?php echo "<?php  echo __('{$singularHumanName}'); ?>"; ?></h4>
			</div>
			<div class="widget-body">
				<table cellpadding="0" cellspacing="0" class="dynamicTable colVis table table-striped table-bordered table-condensed table-white dataTable">
					<tbody>
						<?php
						foreach ($fields as $field) {
							$isKey = false;
							if (!empty($associations['belongsTo'])) {
								foreach ($associations['belongsTo'] as $alias => $details) {
									if ($field === $details['foreignKey']) {
										$isKey = true;
										echo "<tr>";
										echo "\t\t<td><strong><?php echo __('" . Inflector::humanize(Inflector::underscore($alias)) . "'); ?></strong></td>\n";
										echo "\t\t<td>\n\t\t\t<?php echo \$this->Html->link(\${$singularVar}['{$alias}']['{$details['displayField']}'], array('controller' => '{$details['controller']}', 'action' => 'view', \${$singularVar}['{$alias}']['{$details['primaryKey']}']), array('class' => '')); ?>\n\t\t\t&nbsp;\n\t\t</td>\n";
										echo "</tr>";
										break;
									}
								}
							}
							if ($isKey !== true) {
								echo "<tr>";
								echo "\t\t<td><strong><?php echo __('" . Inflector::humanize($field) . "'); ?></strong></td>\n";
								echo "\t\t<td>\n\t\t\t<?php echo h(\${$singularVar}['{$modelClass}']['{$field}']); ?>\n\t\t\t&nbsp;\n\t\t</td>\n";
								echo "</tr>";
							}
						}
						?>
					</tbody>
				</table>
			</div><!-- /.table-responsive -->
	
	</div><!-- /#page-content .col-sm-9 -->

</div><!-- /#content .row-fluid -->
