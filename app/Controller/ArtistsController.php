<?php
App::uses('AppController', 'Controller');
/**
 * Artists Controller
 *
 * @property Artist $Artist
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class ArtistsController extends AppController {

	public $components = array('Paginator', 'Flash', 'Session', 'Auth');
	public $helpers = array('Wysiwyg.Wysiwyg' => array('editor' => 'Ck'));
	public $uses = array('Artist','Traduc');
	public function beforeFilter() {
		$this->Auth->Allow('login');
		$this->Auth->logoutRedirect = array('/admin/users/login');
		
		$this->Auth->authenticate = array(
		    AuthComponent::ALL => array('userModel' => 'User'),
		    'Form'=> array(
                'fields' => array('username' => 'email'),
		    'Basic'));
		$this->Auth->authError = "Please log in first in order to preform that action.";


		if(isset($this->params['url']['lang'])){
			$this->Session->write('Lang.idioma', $this->params['url']['lang']);

		}
		if(!$this->Session->check('Lang.idioma')){
			$this->Session->write('Lang.idioma', 'esp');
		}
		$this->Artist->locale = $this->Session->read('Lang.idioma');
		
	}
	public function beforeRender() {
		
		$this->set('idioma', $this->Session->read('Lang.idioma'));
		
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {

		$this->Artist->recursive = 0;
		$this->set('artists', $this->Artist->find('all',array(
			'order' => 'sort_order ASC'
			)
		));
		
	}


	public function reorder() {
		foreach ($this->data['Artist'] as $key => $value) {
			$this->Artist->id = $value;
			$this->Artist->saveField("sort_order",$key + 1);
		}
		//$this->log(print_r($this->data,true));
		exit();
	}

	public function admin_ann(){
		App::import('Controller', 'Bonzzays');
		$bonzzay = new BonzzaysController;
		$bonzzay->constructClasses();
		$bonzzay->artistsadd();
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			
				//$this->Artist->locale = array('esp','eng','fra','por','ger','chi','ita','rus','jpn');
				//$this->Artist->locale = null;
				//$this->Artist->locale('esp','eng','fra','por','ger','chi','ita','rus','jpn');
				$this->Artist->locale = 'esp';
				$this->Artist->create();
				if ($this->Artist->save($this->request->data)) {
					$lastid = $this->Artist->getLastInsertID();
					$this->admin_artists($lastid );
					$this->Session->setFlash(__('The artist has been saved'), 'flash/success');
					$this->redirect(array('action' => 'index'));
				} else {
					$this->Session->setFlash(__('The artist could not be saved. Please, try again.'), 'flash/error');
				}
			
			
			
		}
		$typeartists = $this->Artist->typeArtists();
		$this->set('typeartists', $typeartists);
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
        $this->Artist->id = $id;
		if (!$this->Artist->exists($id)) {
			throw new NotFoundException(__('Invalid artist'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if(!isset($this->request->data['Artist']['isactive'])){
				$this->request->data['Artist']['isactive']=0;
			}
			if ($this->Artist->save($this->request->data)) {
				$this->Session->setFlash(__('The artist has been saved'), 'flash/success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The artist could not be saved. Please, try again.'), 'flash/error');
			}
		} else {
			$options = array('conditions' => array('Artist.' . $this->Artist->primaryKey => $id));
			$this->request->data = $this->Artist->find('first', $options);
		}
		$typeartists = $this->Artist->typeArtists();
		$this->set('typeartists', $typeartists);
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Artist->id = $id;
		if (!$this->Artist->exists()) {
			throw new NotFoundException(__('Invalid artist'));
		}
		if ($this->Artist->delete()) {
			$this->Session->setFlash(__('Artist deleted'), 'flash/success');
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Artist was not deleted'), 'flash/error');
		$this->redirect(array('action' => 'index'));
	}

	private function admin_artists($id = null)
	{

		


	    $this->Artist->Behaviors->disable('Translate'); 
	    $out = $this->Artist->find('all', array('conditions'=>array('Artist.id'=>$id), 'recursive'=>-1, 'order'=>'id'));
	    $langs = array( 1 => 'eng', 2=>'fra', 3 =>'por', 4 =>'ger', 5 => 'chi', 6 =>'ita', 7 => 'rus', 8 => 'jpn');
	  

	    foreach($out as $v){
	    		foreach($langs as $lang){
	    	
	    	
		        $name['locale'] = $lang;
		        $name['model'] = 'Artist';
		        $name['foreign_key'] = $v['Artist']['id'];
		        $name['field'] = 'name';
		        $name['content'] = $v['Artist']['name'];
		        $this->Traduc->query("INSERT INTO `i18n`(`locale`, `model`, `foreign_key`, `field`, `content`) VALUES ('".$name['locale']."', '".$name['model']."', '".$name['foreign_key']."', '".$name['field']."', '".$name['content']."')");


		       

		        $slug['locale'] = $lang;
		        $slug['model'] = 'Artist';
		        $slug['foreign_key'] = $v['Artist']['id'];
		        $slug['field'] = 'slug';
		        $slug['content'] = $v['Artist']['slug'];
		        $this->Traduc->query("INSERT INTO `i18n`(`locale`, `model`, `foreign_key`, `field`, `content`) VALUES ('".$slug['locale']."', '".$slug['model']."', '".$slug['foreign_key']."', '".$slug['field']."', '".$slug['content']."')");
		     
		        $small_description['locale'] = $lang;
		        $small_description['model'] = 'Artist';
		        $small_description['foreign_key'] = $v['Artist']['id'];
		        $small_description['field'] = 'small_description';
		        $small_description['content'] = $v['Artist']['small_description'];
		        $this->Traduc->query("INSERT INTO `i18n`(`locale`, `model`, `foreign_key`, `field`, `content`) VALUES ('".$small_description['locale']."', '".$small_description['model']."', '".$small_description['foreign_key']."', '".$small_description['field']."', '".$small_description['content']."')");

				$description['locale'] = $lang;
		        $description['model'] = 'Artist';
		        $description['foreign_key'] = $v['Artist']['id'];
		        $description['field'] = 'description';
		        $description['content'] = $v['Artist']['description'];
		        $this->Traduc->query("INSERT INTO `i18n`(`locale`, `model`, `foreign_key`, `field`, `content`) VALUES ('".$description['locale']."', '".$description['model']."', '".$description['foreign_key']."', '".$description['field']."', '".$description['content']."')");

		        $seo_title['locale'] = $lang;
		        $seo_title['model'] = 'Artist';
		        $seo_title['foreign_key'] = $v['Artist']['id'];
		        $seo_title['field'] = 'seo_title';
		        $seo_title['content'] = $v['Artist']['seo_title'];
		        $this->Traduc->query("INSERT INTO `i18n`(`locale`, `model`, `foreign_key`, `field`, `content`) VALUES ('".$name['locale']."', '".$seo_title['model']."', '".$seo_title['foreign_key']."', '".$seo_title['field']."', '".$seo_title['content']."')");

				$seo_description['locale'] = $lang;
		        $seo_description['model'] = 'Artist';
		        $seo_description['foreign_key'] = $v['Artist']['id'];
		        $seo_description['field'] = 'seo_description';
		        $seo_description['content'] = $v['Artist']['seo_description'];
		       $this->Traduc->query("INSERT INTO `i18n`(`locale`, `model`, `foreign_key`, `field`, `content`) VALUES ('".$seo_description['locale']."', '".$seo_description['model']."', '".$seo_description['foreign_key']."', '".$seo_description['field']."', '".$seo_description['content']."')");

		        $seo_keys['locale'] = $lang;
		        $seo_keys['model'] = 'Artist';
		        $seo_keys['foreign_key'] = $v['Artist']['id'];
		        $seo_keys['field'] = 'seo_keys';
		        $seo_keys['content'] = $v['Artist']['seo_keys'];
		        $this->Traduc->query("INSERT INTO `i18n`(`locale`, `model`, `foreign_key`, `field`, `content`) VALUES ('".$seo_keys['locale']."', '".$seo_keys['model']."', '".$seo_keys['foreign_key']."', '".$seo_keys['field']."', '".$seo_keys['content']."')");
		        
		    }


	    }

	   
	    
	    
	}
}
