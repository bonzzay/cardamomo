<?php
App::uses('AppController', 'Controller');
/**
 * Artists Controller
 *
 * @property Artist $Artist
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class BonzzaysController extends AppController {

	public $components = array('Paginator', 'Flash', 'Session', 'Auth');
	
	public $uses = array('Artist', 'Content', 'Menu', 'Entry');

	public function beforeFilter() {
		$this->Auth->Allow('login');
		$this->Auth->logoutRedirect = array('/admin/users/login');
		
		$this->Auth->authenticate = array(
		    AuthComponent::ALL => array('userModel' => 'User'),
		    'Form'=> array(
                'fields' => array('username' => 'email'),
		    'Basic'));
		$this->Auth->authError = "Please log in first in order to preform that action.";
		
	}
	public function beforeRender() {
	}
	

	function artistsadd()
	{
	    $langs = array(0 => 'esp', 1 => 'eng', 2=>'fra', 3 =>'por', 4 =>'ger', 5 => 'chi', 6 =>'ita', 7 => 'rus', 8 => 'jpn');
		foreach($langs as $lang){
			$this->Artist->Behaviors->disable('Translate'); 
	    	$out = $this->Artist->find('all', array('recursive'=>-1, 'order'=>'id'));
	    	var_dump($out);
	    	foreach($out as $v){
	    	
	    	
	    		$db = ConnectionManager::getDataSource('default');
		        $name['locale'] = $lang;
		        $name['model'] = 'Artist';
		        $name['foreign_key'] = $v['Artist']['id'];
		        $name['field'] = 'name';
		        $name['content'] = $v['Artist']['name'];
		        $db->rawQuery("INSERT INTO `i18n`(`locale`, `model`, `foreign_key`, `field`, `content`) VALUES ('".$name['locale']."', '".$name['model']."', '".$name['foreign_key']."', '".$name['field']."', '".$name['content']."')");


		       

		        $slug['locale'] = $lang;
		        $slug['model'] = 'Artist';
		        $slug['foreign_key'] = $v['Artist']['id'];
		        $slug['field'] = 'slug';
		        $slug['content'] = $v['Artist']['slug'];
		        $db->rawQuery("INSERT INTO `i18n`(`locale`, `model`, `foreign_key`, `field`, `content`) VALUES ('".$slug['locale']."', '".$slug['model']."', '".$slug['foreign_key']."', '".$slug['field']."', '".$slug['content']."')");
		     
		        $small_description['locale'] = $lang;
		        $small_description['model'] = 'Artist';
		        $small_description['foreign_key'] = $v['Artist']['id'];
		        $small_description['field'] = 'small_description';
		        $small_description['content'] = $v['Artist']['small_description'];
		        $db->rawQuery("INSERT INTO `i18n`(`locale`, `model`, `foreign_key`, `field`, `content`) VALUES ('".$small_description['locale']."', '".$small_description['model']."', '".$small_description['foreign_key']."', '".$small_description['field']."', '".$small_description['content']."')");

				$description['locale'] = $lang;
		        $description['model'] = 'Artist';
		        $description['foreign_key'] = $v['Artist']['id'];
		        $description['field'] = 'description';
		        $description['content'] = $v['Artist']['description'];
		        $db->rawQuery("INSERT INTO `i18n`(`locale`, `model`, `foreign_key`, `field`, `content`) VALUES ('".$description['locale']."', '".$description['model']."', '".$description['foreign_key']."', '".$description['field']."', '".$description['content']."')");

		        $seo_title['locale'] = $lang;
		        $seo_title['model'] = 'Artist';
		        $seo_title['foreign_key'] = $v['Artist']['id'];
		        $seo_title['field'] = 'seo_title';
		        $seo_title['content'] = $v['Artist']['seo_title'];
		        $db->rawQuery("INSERT INTO `i18n`(`locale`, `model`, `foreign_key`, `field`, `content`) VALUES ('".$name['locale']."', '".$seo_title['model']."', '".$seo_title['foreign_key']."', '".$seo_title['field']."', '".$seo_title['content']."')");

				$seo_description['locale'] = $lang;
		        $seo_description['model'] = 'Artist';
		        $seo_description['foreign_key'] = $v['Artist']['id'];
		        $seo_description['field'] = 'seo_description';
		        $seo_description['content'] = $v['Artist']['seo_description'];
		       	$db->rawQuery("INSERT INTO `i18n`(`locale`, `model`, `foreign_key`, `field`, `content`) VALUES ('".$seo_description['locale']."', '".$seo_description['model']."', '".$seo_description['foreign_key']."', '".$seo_description['field']."', '".$seo_description['content']."')");

		        $seo_keys['locale'] = $lang;
		        $seo_keys['model'] = 'Artist';
		        $seo_keys['foreign_key'] = $v['Artist']['id'];
		        $seo_keys['field'] = 'seo_keys';
		        $seo_keys['content'] = $v['Artist']['seo_keys'];
		        $db->rawQuery("INSERT INTO `i18n`(`locale`, `model`, `foreign_key`, `field`, `content`) VALUES ('".$seo_keys['locale']."', '".$seo_keys['model']."', '".$seo_keys['foreign_key']."', '".$seo_keys['field']."', '".$seo_keys['content']."')");
		       	
		    }
		    
		}
		die();
	   //$this->redirect('/admin/artists/');
	    
	    
	}

	public function contentsadd()
	{
	    $langs = array(0 => 'esp', 1 => 'eng', 2=>'fra', 3 =>'por', 4 =>'ger', 5 => 'chi', 6 =>'ita', 7 => 'rus', 8 => 'jpn');
	    foreach($langs as $lang){
	    	$this->Content->Behaviors->disable('Translate'); 
	    	$out = $this->Content->find('all', array('recursive'=>-1, 'order'=>'id'));
	    	foreach($out as $v){
	    	

	    		$db = ConnectionManager::getDataSource('default');
	    		
		        $title['locale'] = $lang;
		        $title['model'] = 'Content';
		        $title['foreign_key'] = $v['Content']['id'];
		        $title['field'] = 'title';
		        $title['content'] = $v['Content']['title'];
		        $db->rawQuery("INSERT INTO `i18n`(`locale`, `model`, `foreign_key`, `field`, `content`) VALUES ('".$title['locale']."', '".$title['model']."', '".$title['foreign_key']."', '".$title['field']."', '".$title['content']."')");

		        $slug['locale'] = $lang;
		        $slug['model'] = 'Content';
		        $slug['foreign_key'] = $v['Content']['id'];
		        $slug['field'] = 'slug';
		        $slug['content'] = $v['Content']['slug'];
		        $db->rawQuery("INSERT INTO `i18n`(`locale`, `model`, `foreign_key`, `field`, `content`) VALUES ('".$slug['locale']."', '".$slug['model']."', '".$slug['foreign_key']."', '".$slug['field']."', '".$slug['content']."')");

		        $text['locale'] = $lang;
		        $text['model'] = 'Content';
		        $text['foreign_key'] = $v['Content']['id'];
		        $text['field'] = 'text';
		        $text['content'] = $v['Content']['text'];
		        $db->rawQuery("INSERT INTO `i18n`(`locale`, `model`, `foreign_key`, `field`, `content`) VALUES ('".$text['locale']."', '".$text['model']."', '".$text['foreign_key']."', '".$text['field']."', '".$text['content']."')");


				
		        $seo_title['locale'] = $lang;
		        $seo_title['model'] = 'Content';
		        $seo_title['foreign_key'] = $v['Content']['id'];
		        $seo_title['field'] = 'seo_title';
		        $seo_title['content'] = $v['Content']['seo_title'];
		        $db->rawQuery("INSERT INTO `i18n`(`locale`, `model`, `foreign_key`, `field`, `content`) VALUES ('".$seo_title['locale']."', '".$seo_title['model']."', '".$seo_title['foreign_key']."', '".$seo_title['field']."', '".$seo_title['content']."')");

				$seo_description['locale'] = $lang;
		        $seo_description['model'] = 'Content';
		        $seo_description['foreign_key'] = $v['Content']['id'];
		        $seo_description['field'] = 'seo_description';
		        $seo_description['content'] = $v['Content']['seo_description'];
		        $db->rawQuery("INSERT INTO `i18n`(`locale`, `model`, `foreign_key`, `field`, `content`) VALUES ('".$seo_description['locale']."', '".$seo_description['model']."', '".$seo_description['foreign_key']."', '".$seo_description['field']."', '".$seo_description['content']."')");

		        $seo_keys['locale'] = $lang;
		        $seo_keys['model'] = 'Content';
		        $seo_keys['foreign_key'] = $v['Content']['id'];
		        $seo_keys['field'] = 'seo_keys';
		        $seo_keys['content'] = $v['Content']['seo_keys'];
		        $db->rawQuery("INSERT INTO `i18n`(`locale`, `model`, `foreign_key`, `field`, `content`) VALUES ('".$seo_keys['locale']."', '".$seo_keys['model']."', '".$seo_keys['foreign_key']."', '".$seo_keys['field']."', '".$seo_keys['content']."')");
	    	}
		}
		die();

	    
	}

	function entriesadd()
	{
	    $langs = array(0 => 'esp', 1 => 'eng', 2=>'fra', 3 =>'por', 4 =>'ger', 5 => 'chi', 6 =>'ita', 7 => 'rus', 8 => 'jpn');
	    foreach($langs as $lang){
	    	$this->Entry->Behaviors->disable('Translate'); 
	    	$out = $this->Entry->find('all', array('recursive'=>-1, 'order'=>'id'));
	    	 foreach($out as $v){
	    	 	$db = ConnectionManager::getDataSource('default');
		        $name['locale'] = $lang;
		        $name['model'] = 'Entry';
		        $name['foreign_key'] = $v['Entry']['id'];
		        $name['field'] = 'name';
		        $name['content'] = $v['Entry']['name'];
		        $db->rawQuery("INSERT INTO `i18n`(`locale`, `model`, `foreign_key`, `field`, `content`) VALUES ('".$name['locale']."', '".$name['model']."', '".$name['foreign_key']."', '".$name['field']."', '".$name['content']."')");

		        $title['locale'] = $lang;
		        $title['model'] = 'Entry';
		        $title['foreign_key'] = $v['Entry']['id'];
		        $title['field'] = 'title';
		        $title['content'] = $v['Entry']['title'];
		        $db->rawQuery("INSERT INTO `i18n`(`locale`, `model`, `foreign_key`, `field`, `content`) VALUES ('".$title['locale']."', '".$title['model']."', '".$title['foreign_key']."', '".$title['field']."', '".$title['content']."')");

		        $description['locale'] = $lang;
		        $description['model'] = 'Entry';
		        $description['foreign_key'] = $v['Entry']['id'];
		        $description['field'] = 'description';
		        $description['content'] = $v['Entry']['description'];
		        $db->rawQuery("INSERT INTO `i18n`(`locale`, `model`, `foreign_key`, `field`, `content`) VALUES ('".$description['locale']."', '".$description['model']."', '".$description['foreign_key']."', '".$description['field']."', '".$description['content']."')");

		    }


	    }
	    die();
	    
	}

	function menusadd()
	{
	    $langs = array(0 => 'esp', 1 => 'eng', 2=>'fra', 3 =>'por', 4 =>'ger', 5 => 'chi', 6 =>'ita', 7 => 'rus', 8 => 'jpn');
	    foreach($langs as $lang){
	    	$this->Menu->Behaviors->disable('Translate'); 
	    	$out = $this->Menu->find('all', array('recursive'=>-1, 'order'=>'id'));
	    	foreach($out as $v){
	    		$db = ConnectionManager::getDataSource('default');
		        $name['locale'] = $lang;
		        $name['model'] = 'Menu';
		        $name['foreign_key'] = $v['Menu']['id'];
		        $name['field'] = 'name';
		        $name['content'] = $v['Menu']['name'];
		        $db->rawQuery("INSERT INTO `i18n`(`locale`, `model`, `foreign_key`, `field`, `content`) VALUES ('".$name['locale']."', '".$name['model']."', '".$name['foreign_key']."', '".$name['field']."', '".$name['content']."')");

		        $url['locale'] = $lang;
		        $url['model'] = 'Menu';
		        $url['foreign_key'] = $v['Menu']['id'];
		        $url['field'] = 'url';
		        $url['content'] = $v['Menu']['url'];
		        $db->rawQuery("INSERT INTO `i18n`(`locale`, `model`, `foreign_key`, `field`, `content`) VALUES ('".$url['locale']."', '".$url['model']."', '".$url['foreign_key']."', '".$url['field']."', '".$url['content']."')");
		        
		    }
	    }
	    die();
	}

}
