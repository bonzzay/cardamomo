<?php
App::uses('AppController', 'Controller');
/**
 * Contents Controller
 *
 * @property Content $Content
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class ContentsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Flash', 'Session', 'Auth');
	public $helpers = array('Wysiwyg.Wysiwyg' => array('editor' => 'Ck'));
	public $uses = array('Content','Menu', 'Social', 'Traduc');
	public function beforeFilter() {
		$this->Auth->Allow('login');
		$this->Auth->logoutRedirect = array('/admin/users/login');
		
		$this->Auth->authenticate = array(
		    AuthComponent::ALL => array('userModel' => 'User'),
		    'Form'=> array(
                'fields' => array('username' => 'email'),
		    'Basic'));
		$this->Auth->authError = "Please log in first in order to preform that action.";
		if(isset($this->params['url']['lang'])){
			$this->Session->write('Lang.idioma', $this->params['url']['lang']);

		}
		if(!$this->Session->check('Lang.idioma')){
			$this->Session->write('Lang.idioma', 'esp');
		}
		$this->Content->locale = $this->Session->read('Lang.idioma');
		
	}
	public function beforeRender() {
		
		$this->set('idioma', $this->Session->read('Lang.idioma'));
		
	}

	public function admin_ann(){
		App::import('Controller', 'Bonzzays');
		$bonzzay = new BonzzaysController;
		$bonzzay->constructClasses();
		$bonzzay->contentsadd();
	}



/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {

		$this->Content->recursive = 0;
		$this->set('contents', $this->paginate());
	}



/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
		
			$this->Content->create();
			if ($this->Content->save($this->request->data)) {
				$lastid = $this->Content->getLastInsertID();
				$this->admin_contents($lastid );
				$this->Session->setFlash(__('The content has been saved'), 'flash/success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The content could not be saved. Please, try again.'), 'flash/error');
			}
		}
		$galeries = $this->Content->Galery->find('list');
		$sliders = $this->Content->Slider->find('list');
		$this->set(compact('galeries', 'sliders'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
        $this->Content->id = $id;
		if (!$this->Content->exists($id)) {
			throw new NotFoundException(__('Invalid content'));
		} 
		if ($this->request->is('post') || $this->request->is('put')) {
			
			if(!isset($this->request->data['Content']['isactive'])){
				$this->request->data['Content']['isactive']=0;
			}
			if ($this->Content->save($this->request->data)) {
				$this->Session->setFlash(__('The content has been saved'), 'flash/success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The content could not be saved. Please, try again.'), 'flash/error');
			}
		} else {
			$options = array('conditions' => array('Content.' . $this->Content->primaryKey => $id));
			$this->request->data = $this->Content->find('first', $options);
		}
		$galeries = $this->Content->Galery->find('list');
		$sliders = $this->Content->Slider->find('list');
		$this->set(compact('galeries', 'sliders'));
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		$this->theme = "Cardamomo";
		if (!$this->Content->exists($id)) {
			throw new NotFoundException(__('Invalid entry'));
		}
		$this->set('title_for_layout', 'Cardamomo');
		$this->set('description_for_layout', 'Cardamomo, tablao flamenco');
		$this->set('keywords_for_layout', 'Tablo flamenco, flamenco madrid, tablo madrid');
		//top menu
		$topmenu = $this->Menu->find('all', array('conditions'=> array('Menu.position' => 1), 'order' => array('Menu.sort_order ASC')));
		$this->set('topmenus',$topmenu) ;


		//Footer
		$topfooter = $this->Menu->find('all', array('conditions'=> array('Menu.position' => 2), 'order' => array('Menu.sort_order ASC')));
		$this->set('topfooter',$topfooter) ;

		//subfooter
		$subfooter = $this->Menu->find('all', array('conditions'=> array('Menu.position' => 3), 'order' => array('Menu.sort_order ASC')));
		$this->set('subfooter',$subfooter) ;


		//render social
		
		$this->set('socials', $this->Social->find('all', array('order' => array('Social.sort_order ASC'))));
		$this->Content->recursive = 2;
		$options = array('conditions' => array('Content.' . $this->Content->primaryKey => $id));
		$this->set('content', $this->Content->find('first', $options));
		$this->render('/pages/content');
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Content->id = $id;
		if (!$this->Content->exists()) {
			throw new NotFoundException(__('Invalid content'));
		}
		if ($this->Content->delete()) {
			$this->Session->setFlash(__('Content deleted'), 'flash/success');
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Content was not deleted'), 'flash/error');
		$this->redirect(array('action' => 'index'));
	}

	private function admin_contents($id =null)
	{

		


	    $this->Content->Behaviors->disable('Translate'); 
	    $out = $this->Content->find('all', array('conditions'=>array('Content.id'=>$id), 'recursive'=>-1, 'order'=>'id'));
	    $langs = array( 1 => 'eng', 2=>'fra', 3 =>'por', 4 =>'ger', 5 => 'chi', 6 =>'ita', 7 => 'rus', 8 => 'jpn');

	    foreach($out as $v){
	    	
	    	foreach($langs as $lang){
	    		
		        $title['locale'] = $lang;
		        $title['model'] = 'Content';
		        $title['foreign_key'] = $v['Content']['id'];
		        $title['field'] = 'title';
		        $title['content'] = $v['Content']['title'];
		        $this->Traduc->query("INSERT INTO `i18n`(`locale`, `model`, `foreign_key`, `field`, `content`) VALUES ('".$title['locale']."', '".$title['model']."', '".$title['foreign_key']."', '".$title['field']."', '".$title['content']."')");

		        $slug['locale'] = $lang;
		        $slug['model'] = 'Content';
		        $slug['foreign_key'] = $v['Content']['id'];
		        $slug['field'] = 'slug';
		        $slug['content'] = $v['Content']['slug'];
		        $this->Traduc->query("INSERT INTO `i18n`(`locale`, `model`, `foreign_key`, `field`, `content`) VALUES ('".$slug['locale']."', '".$slug['model']."', '".$slug['foreign_key']."', '".$slug['field']."', '".$slug['content']."')");

		        $text['locale'] = $lang;
		        $text['model'] = 'Content';
		        $text['foreign_key'] = $v['Content']['id'];
		        $text['field'] = 'text';
		        $text['content'] = $v['Content']['text'];
		        $this->Traduc->query("INSERT INTO `i18n`(`locale`, `model`, `foreign_key`, `field`, `content`) VALUES ('".$text['locale']."', '".$text['model']."', '".$text['foreign_key']."', '".$text['field']."', '".$text['content']."')");


				
		        $seo_title['locale'] = $lang;
		        $seo_title['model'] = 'Content';
		        $seo_title['foreign_key'] = $v['Content']['id'];
		        $seo_title['field'] = 'seo_title';
		        $seo_title['content'] = $v['Content']['seo_title'];
		        $this->Traduc->query("INSERT INTO `i18n`(`locale`, `model`, `foreign_key`, `field`, `content`) VALUES ('".$seo_title['locale']."', '".$seo_title['model']."', '".$seo_title['foreign_key']."', '".$seo_title['field']."', '".$seo_title['content']."')");

				$seo_description['locale'] = $lang;
		        $seo_description['model'] = 'Content';
		        $seo_description['foreign_key'] = $v['Content']['id'];
		        $seo_description['field'] = 'seo_description';
		        $seo_description['content'] = $v['Content']['seo_description'];
		        $this->Traduc->query("INSERT INTO `i18n`(`locale`, `model`, `foreign_key`, `field`, `content`) VALUES ('".$seo_description['locale']."', '".$seo_description['model']."', '".$seo_description['foreign_key']."', '".$seo_description['field']."', '".$seo_description['content']."')");

		        $seo_keys['locale'] = $lang;
		        $seo_keys['model'] = 'Content';
		        $seo_keys['foreign_key'] = $v['Content']['id'];
		        $seo_keys['field'] = 'seo_keys';
		        $seo_keys['content'] = $v['Content']['seo_keys'];
		        $this->Traduc->query("INSERT INTO `i18n`(`locale`, `model`, `foreign_key`, `field`, `content`) VALUES ('".$seo_keys['locale']."', '".$seo_keys['model']."', '".$seo_keys['foreign_key']."', '".$seo_keys['field']."', '".$seo_keys['content']."')");
	    	}
		}

	    
	}
}
