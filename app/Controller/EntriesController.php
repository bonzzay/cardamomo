<?php
App::uses('AppController', 'Controller');
/**
 * Entries Controller
 *
 * @property Entry $Entry
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class EntriesController extends AppController {

	public $components = array('Paginator', 'Flash', 'Session', 'Auth');
	public $helpers = array('Wysiwyg.Wysiwyg' => array('editor' => 'Ck'));
	public $uses = array('Entry','Traduc');

	public function beforeFilter() {
		$this->Auth->Allow('login');
		$this->Auth->logoutRedirect = array('/admin/users/login');
		
		$this->Auth->authenticate = array(
		    AuthComponent::ALL => array('userModel' => 'User'),
		    'Form'=> array(
                'fields' => array('username' => 'email'),
		    'Basic'));
		$this->Auth->authError = "Please log in first in order to preform that action.";

		if(isset($this->params['url']['lang'])){
			$this->Session->write('Lang.idioma', $this->params['url']['lang']);

		}
		if(!$this->Session->check('Lang.idioma')){
			$this->Session->write('Lang.idioma', 'esp');
		}
		$this->Entry->locale = $this->Session->read('Lang.idioma');
		
	}
	public function beforeRender() {
		
		$this->set('idioma', $this->Session->read('Lang.idioma'));
		
	}

	public function admin_ann(){
		App::import('Controller', 'Bonzzays');
		$bonzzay = new BonzzaysController;
		$bonzzay->constructClasses();
		$bonzzay->entriesadd();
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Entry->recursive = 0;
		$this->set('entries', $this->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Entry->exists($id)) {
			throw new NotFoundException(__('Invalid entry'));
		}
		$options = array('conditions' => array('Entry.' . $this->Entry->primaryKey => $id));
		$this->set('entry', $this->Entry->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Entry->locale = 'esp';
			$this->Entry->create();
			if ($this->Entry->save($this->request->data)) {
				$lastid = $this->Entry->getLastInsertID();
				$this->admin_entries($lastid );
				$this->Session->setFlash(__('The entry has been saved'), 'flash/success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The entry could not be saved. Please, try again.'), 'flash/error');
			}
		}
		$types = $this->Entry->Type->find('list');
		$this->set(compact('types'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
        $this->Entry->id = $id;
		if (!$this->Entry->exists($id)) {
			throw new NotFoundException(__('Invalid entry'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if(!isset($this->request->data['Entry']['isdinner'])){
				$this->request->data['Entry']['isdinner'] = 0;
			}
			if ($this->Entry->save($this->request->data)) {
				$this->Session->setFlash(__('The entry has been saved'), 'flash/success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The entry could not be saved. Please, try again.'), 'flash/error');
			}
		} else {
			$options = array('conditions' => array('Entry.' . $this->Entry->primaryKey => $id));
			$this->request->data = $this->Entry->find('first', $options);
		}
		$types = $this->Entry->Type->find('list');
		$this->set(compact('types'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Entry->id = $id;
		if (!$this->Entry->exists()) {
			throw new NotFoundException(__('Invalid entry'));
		}
		if ($this->Entry->delete()) {
			$this->Session->setFlash(__('Entry deleted'), 'flash/success');
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Entry was not deleted'), 'flash/error');
		$this->redirect(array('action' => 'index'));
	}

	private function admin_entries($id=null)
	{

		


	    $this->Entry->Behaviors->disable('Translate'); 
	    $out = $this->Entry->find('all', array('conditions'=>array('Entry.id'=>$id),'recursive'=>-1, 'order'=>'id'));
	    
	    $langs = array( 1 => 'eng', 2=>'fra', 3 =>'por', 4 =>'ger', 5 => 'chi', 6 =>'ita', 7 => 'rus', 8 => 'jpn');
	    foreach($out as $v){
	    	foreach($langs as $lang){
		        $name['locale'] = $lang;
		        $name['model'] = 'Entry';
		        $name['foreign_key'] = $v['Entry']['id'];
		        $name['field'] = 'name';
		        $name['content'] = $v['Entry']['name'];
		        $this->Traduc->query("INSERT INTO `i18n`(`locale`, `model`, `foreign_key`, `field`, `content`) VALUES ('".$name['locale']."', '".$name['model']."', '".$name['foreign_key']."', '".$name['field']."', '".$name['content']."')");

		        $title['locale'] = $lang;
		        $title['model'] = 'Entry';
		        $title['foreign_key'] = $v['Entry']['id'];
		        $title['field'] = 'title';
		        $title['content'] = $v['Entry']['title'];
		        $this->Traduc->query("INSERT INTO `i18n`(`locale`, `model`, `foreign_key`, `field`, `content`) VALUES ('".$title['locale']."', '".$title['model']."', '".$title['foreign_key']."', '".$title['field']."', '".$title['content']."')");

		        $description['locale'] = $lang;
		        $description['model'] = 'Entry';
		        $description['foreign_key'] = $v['Entry']['id'];
		        $description['field'] = 'description';
		        $description['content'] = $v['Entry']['description'];
		        $this->Traduc->query("INSERT INTO `i18n`(`locale`, `model`, `foreign_key`, `field`, `content`) VALUES ('".$description['locale']."', '".$description['model']."', '".$description['foreign_key']."', '".$description['field']."', '".$description['content']."')");

		    }


	    }
	    $this->redirect('/admin/entries/');
	    
	}
}
