<?php
/*
 * Controller/EventsController.php
 * CakePHP Full Calendar Plugin
 *
 * Copyright (c) 2010 Silas Montgomery
 * http://silasmontgomery.com
 *
 * Licensed under MIT
 * http://www.opensource.org/licenses/mit-license.php
 */

class EventsController extends AppController {
	public $components = array('Paginator', 'Flash', 'Session', 'Auth');

	public $helpers = array('Html', 'Form', 'Session','Wysiwyg.Wysiwyg' => array('editor' => 'Ck'));
	
	var $name = 'Events';
	public function beforeFilter() {
		$this->Auth->Allow('login');
		$this->Auth->logoutRedirect = array('/admin/users/login');
		
		$this->Auth->authenticate = array(
		    AuthComponent::ALL => array('userModel' => 'User'),
		    'Form'=> array(
                'fields' => array('username' => 'email'),
		    'Basic'));
		$this->Auth->authError = "Please log in first in order to preform that action.";

		
	}

    var $paginate = array(
        'limit' => 15
    );

    function admin_index() {
		$this->Event->recursive = 1;
		$this->set('events', $this->paginate());
	}

	/*function admin_view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid event', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('event', $this->Event->read(null, $id));
	}*/

	function admin_add() {
		if (!empty($this->data)) {
			
			$this->Event->create();
			if ($this->Event->save($this->data)) {
				$this->Session->setFlash(__('The event has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The event could not be saved. Please, try again.', true));
			}
		}
		$this->set('types', $this->Event->Type->find('list'));
	}

	function admin_periodic(){
		if (!empty($this->data)) {
			
			$startDate = date("d/m/Y", strtotime($this->data['Event']['start']));
			$endDate = date("d/m/Y", strtotime($this->data['Event']['end']));
			$endhour = date("H:i", strtotime($this->data['Event']['end']));
			$starthour = date("H:i", strtotime($this->data['Event']['start']));


			$inicio=DateTime::createFromFormat('d/m/Y', $startDate);
			$fin   =DateTime::createFromFormat('d/m/Y', $endDate);
			$fechas = $this->getFechas($inicio, $fin, 'P1D', false);
			foreach ($fechas as $key => $value) {
				$data['Event']['start'] = $value.' '.$starthour;
				$data['Event']['end'] = $value.' '.$endhour;
				$data['Event']['type_id'] = $this->data['Event']['type_id'];
				$data['Event']['title'] = $this->data['Event']['title'];
				$data['Event']['details'] = $this->data['Event']['details'];
				//$data['Event']['all_day'] = $this->data['Event']['all_day'];
				//$data['Event']['status'] = $this->data['Event']['status'];
				$data['Event']['quantity'] = $this->data['Event']['quantity'];


				$this->Event->create();
				if ($this->Event->save($data)) {
					$this->Session->setFlash(__('The event has been saved', true));
					
				} else {
					$this->Session->setFlash(__('The event could not be saved. Please, try again.', true));
				}
				
			}
			$this->redirect(array('action' => 'index'));

		
			
			
		}
		$this->set('types', $this->Event->Type->find('list'));
	}

	function admin_edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid event', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->Event->save($this->data)) {
				$this->Session->setFlash(__('The event has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The event could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Event->read(null, $id);
			$this->set('event',$this->data);
		}
		$this->set('types', $this->Event->Type->find('list'));
	}

	function admin_delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for event', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->Event->delete($id)) {
			$this->Session->setFlash(__('Event deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Event was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
	public function admin_deletes()
	{
	    foreach($this->request->data['Event'] as $key => $value)
	    {
	       $this->Event->delete($key);
	    }
	    $this->redirect($this->referer());
	}

	public function admin_close($id = 0, $u = 1)
	{
	   
	    $this->Event->id = $id;
		$this->Event->saveField('active', $u);
	    $this->redirect($this->referer());
	}

	private function getFechas($inicio, $fin, $intervalo, $sundays=false)
	{
	  $_intervalo = new DateInterval($intervalo);
	  $fechas = new DatePeriod($inicio, $_intervalo ,$fin);

	  $ret = array();
	  foreach ($fechas as $fecha) 
	    {
	      if (($sundays) && ($fecha->format('w')==0) )
	    $fecha->modify('+1 day');

	      $ret[]=$fecha->format('Y-m-d');
	    }

	  return $ret;
	}



    // The feed action is called from "webroot/js/ready.js" to get the list of events (JSON)
	/*function feed($id=null) {
		$this->layout = "ajax";
		$vars = $this->params['url'];
		$conditions = array('conditions' => array('UNIX_TIMESTAMP(start) >=' => $vars['start'], 'UNIX_TIMESTAMP(start) <=' => $vars['end']));
		$events = $this->Event->find('all', $conditions);
		foreach($events as $event) {
			if($event['Event']['all_day'] == 1) {
				$allday = true;
				$end = $event['Event']['start'];
			} else {
				$allday = false;
				$end = $event['Event']['end'];
			}
			$data[] = array(
					'id' => $event['Event']['id'],
					'title'=>$event['Event']['title'],
					'start'=>$event['Event']['start'],
					'end' => $end,
					'allDay' => $allday,
					'url' => Router::url('/') . 'full_calendar/events/view/'.$event['Event']['id'],
					'details' => $event['Event']['details'],
					'className' => $event['EventType']['color']
			);
		}
		$this->set("json", json_encode($data));
	}*/

        // The update action is called from "webroot/js/ready.js" to update date/time when an event is dragged or resized
	/*function admin_update() {
		$vars = $this->params['url'];
		$this->Event->id = $vars['id'];
		$this->Event->saveField('start', $vars['start']);
		$this->Event->saveField('end', $vars['end']);
		$this->Event->saveField('all_day', $vars['allday']);
	}*/

}
?>
