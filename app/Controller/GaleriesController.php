<?php
App::uses('AppController', 'Controller');
/**
 * Galeries Controller
 *
 * @property Galery $Galery
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class GaleriesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Flash', 'Session', 'Auth');

	public function beforeFilter() {
		$this->Auth->Allow('login');
		$this->Auth->logoutRedirect = array('/admin/users/login');
		
		$this->Auth->authenticate = array(
		    AuthComponent::ALL => array('userModel' => 'User'),
		    'Form'=> array(
                'fields' => array('username' => 'email'),
		    'Basic'));
		$this->Auth->authError = "Please log in first in order to preform that action.";

		
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Galery->recursive = 0;
		$this->set('galeries', $this->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Galery->exists($id)) {
			throw new NotFoundException(__('Invalid galery'));
		}
		$options = array('conditions' => array('Galery.' . $this->Galery->primaryKey => $id));
		$this->set('galery', $this->Galery->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Galery->create();
			if ($this->Galery->save($this->request->data)) {
				$this->Session->setFlash(__('The galery has been saved'), 'flash/success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The galery could not be saved. Please, try again.'), 'flash/error');
			}
		}
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
        $this->Galery->id = $id;
		if (!$this->Galery->exists($id)) {
			throw new NotFoundException(__('Invalid galery'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Galery->save($this->request->data)) {
				$this->Session->setFlash(__('The galery has been saved'), 'flash/success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The galery could not be saved. Please, try again.'), 'flash/error');
			}
		} else {
			$options = array('conditions' => array('Galery.' . $this->Galery->primaryKey => $id));
			$this->request->data = $this->Galery->find('first', $options);
		}
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Galery->id = $id;
		if (!$this->Galery->exists()) {
			throw new NotFoundException(__('Invalid galery'));
		}
		if ($this->Galery->delete()) {
			$this->Session->setFlash(__('Galery deleted'), 'flash/success');
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Galery was not deleted'), 'flash/error');
		$this->redirect(array('action' => 'index'));
	}
}
