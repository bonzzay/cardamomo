<?php
App::uses('AppController', 'Controller');
/**
 * Images Controller
 *
 * @property Image $Image
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class ImagesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Flash', 'Session', 'Auth');

	public function beforeFilter() {
		$this->Auth->Allow('login');
		$this->Auth->logoutRedirect = array('/admin/users/login');
		
		$this->Auth->authenticate = array(
		    AuthComponent::ALL => array('userModel' => 'User'),
		    'Form'=> array(
                'fields' => array('username' => 'email'),
		    'Basic'));
		$this->Auth->authError = "Please log in first in order to preform that action.";

		
	}




/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add($id = null, $type = null) {
		if ($this->request->is('post')) {
			$this->Image->create();
			if ($this->Image->save($this->request->data)) {
				$this->Session->setFlash(__('The image has been saved'), 'flash/success');
				if(isset($this->request->data['Image']['galery_id'])){
						$this->redirect(array('controller' => 'galeries','action' => 'index'));

				}else{
					$this->redirect(array('controller' => 'sliders','action' => 'index'));
				}
				
			} else {
				$this->Session->setFlash(__('The image could not be saved. Please, try again.'), 'flash/error');
			}
		}
		$this->set('id', $id);
		$this->set('type', $type);// 1 Gallery, 2 Slider

		//$sliders = $this->Image->Slider->find('list');
		//$galeries = $this->Image->Galery->find('list');
		//$this->set(compact('sliders', 'galeries'));
	}


/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null, $type = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Image->id = $id;
		if (!$this->Image->exists()) {
			throw new NotFoundException(__('Invalid image'));
		}
		if ($this->Image->delete()) {
			$this->Session->setFlash(__('Image deleted'), 'flash/success');
			if($type == 1){
				$this->redirect(array('controller' => 'galeries','action' => 'index'));

			}else{
				$this->redirect(array('controller' => 'sliders','action' => 'index'));
			}
		}
		$this->Session->setFlash(__('Image was not deleted'), 'flash/error');
		if($type == 1){
			$this->redirect(array('controller' => 'galeries','action' => 'index'));

		}else{
			$this->redirect(array('controller' => 'sliders','action' => 'index'));
		}
	}
}
