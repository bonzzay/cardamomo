<?php
App::uses('AppController', 'Controller');
/**
 * Menus Controller
 *
 * @property Menu $Menu
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class MenusController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Flash', 'Session', 'Auth');
	public $uses = array('Menu', 'Content','Traduc');

	public function beforeFilter() {
		$this->Auth->Allow('login');
		$this->Auth->logoutRedirect = array('/admin/users/login');
		
		$this->Auth->authenticate = array(
		    AuthComponent::ALL => array('userModel' => 'User'),
		    'Form'=> array(
                'fields' => array('username' => 'email'),
		    'Basic'));
		$this->Auth->authError = "Please log in first in order to preform that action.";

		if(isset($this->params['url']['lang'])){
			$this->Session->write('Lang.idioma', $this->params['url']['lang']);

		}
		if(!$this->Session->check('Lang.idioma')){
			$this->Session->write('Lang.idioma', 'esp');
		}
		$this->Menu->locale = $this->Session->read('Lang.idioma');
		
	}

	public function beforeRender() {
		
		$this->set('idioma', $this->Session->read('Lang.idioma'));
		
	}

	public function admin_ann(){
		App::import('Controller', 'Bonzzays');
		$bonzzay = new BonzzaysController;
		$bonzzay->constructClasses();
		$bonzzay->menusadd();
	}


/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index($id = null) {

		$this->Menu->recursive = 0;
		$this->set('menus', $this->Menu->find('all',array(
			'order' => 'sort_order ASC','conditions'=> array('Menu.position'=> $id)
			)
		));
		if($id == 1){
			$this->set('menuname', 'Menu Top');
			$this->set('position', $id);
		}else if($id == 2){
			$this->set('menuname', 'Menu Footer');
			$this->set('position', $id);
		}else if($id == 3){
			$this->set('menuname', 'Menu Subfooter');
			$this->set('position', $id);
		}else if($id == 4){
			$this->set('menuname', 'Menu Galería');
			$this->set('position', $id);
		}else if($id == 5){
			$this->set('menuname', 'Menu Home');
			$this->set('position', $id);
		}

		
	}

	public function reorder() {
		foreach ($this->data['Menu'] as $key => $value) {
			$this->Menu->id = $value;
			$this->Menu->saveField("sort_order",$key + 1);
		}
		//$this->log(print_r($this->data,true));
		exit();
	}


/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add($pos = null) {
		if ($this->request->is('post')) {
			$this->Menu->locale = 'esp';
			$this->Menu->create();

			$data = $this->creaturl($this->request->data['Menu']['link']);
			$this->request->data['Menu']['model'] = $data['Menu']['model'];
			$this->request->data['Menu']['foreign_key'] = $data['Menu']['foreign_key'];
			$this->request->data['Menu']['url'] = $data['Menu']['url'];
			
			if ($this->Menu->save($this->request->data)) {
				$lastid = $this->Menu->getLastInsertID();
				$this->admin_menus($lastid );
				$this->Session->setFlash(__('The menu has been saved'), 'flash/success');
				//$this->redirect(array('action' => 'index'));
				$this->redirect('/admin/menus/'.$this->request->data['Menu']['position']);
			} else {
				$this->Session->setFlash(__('The menu could not be saved. Please, try again.'), 'flash/error');
			}
		}
		$this->set('pos', $pos);
		$this->Content->locale = $this->Session->read('Lang.idioma');
		$this->set('contents', $this->Content->find('all'));
		$this->set('statics', $this->Menu->staticContent());
		
		
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
        $this->Menu->id = $id;
		if (!$this->Menu->exists($id)) {
			throw new NotFoundException(__('Invalid menu'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {

			$data = $this->creaturl($this->request->data['Menu']['link']);
			$this->request->data['Menu']['model'] = $data['Menu']['model'];
			$this->request->data['Menu']['foreign_key'] = $data['Menu']['foreign_key'];
			$this->request->data['Menu']['url'] = $data['Menu']['url'];

			if ($this->Menu->save($this->request->data)) {
				$this->Session->setFlash(__('The menu has been saved'), 'flash/success');
				$this->redirect('/admin/menus/'.$this->request->data['Menu']['position']);
			} else {
				$this->Session->setFlash(__('The menu could not be saved. Please, try again.'), 'flash/error');
			}
		} else {
			$options = array('conditions' => array('Menu.' . $this->Menu->primaryKey => $id));
			$data = $this->request->data = $this->Menu->find('first', $options);
			$this->set('menu', $data);
		}
		$this->Content->locale = $this->Session->read('Lang.idioma');
		$this->set('contents', $this->Content->find('all'));
		$this->set('statics', $this->Menu->staticContent());
		
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null, $pos = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Menu->id = $id;
		if (!$this->Menu->exists()) {
			throw new NotFoundException(__('Invalid menu'));
		}
		if ($this->Menu->delete()) {
			$this->Session->setFlash(__('Menu deleted'), 'flash/success');
			$this->redirect('/admin/menus/'.$pos);
		}
		$this->Session->setFlash(__('Menu was not deleted'), 'flash/error');
		$this->redirect('/admin/menus/'.$pos);
	}

	private function creaturl($link){
		$porciones = explode("#", $link);
	
		
		if(isset($porciones[2])){
			
			$data['Menu']['model'] = $porciones[1];
			$data['Menu']['foreign_key'] = $porciones[2];
			$data['Menu']['url'] = '';
		}else{

			$data['Menu']['model'] = '';
			$data['Menu']['foreign_key'] = '';
			$data['Menu']['url'] = $porciones[0];
		}
		
		return $data;

	}

	private function admin_menus($id=null)
	{

		


	    $this->Menu->Behaviors->disable('Translate'); 
	    $out = $this->Menu->find('all', array('conditions'=>array('Menu.id'=>$id),'recursive'=>-1, 'order'=>'id'));
	    $t = $b = $c= $d= $e = $f = $g =0;
	    $langs = array(1 => 'eng', 2=>'fra', 3 =>'por', 4 =>'ger', 5 => 'chi', 6 =>'ita', 7 => 'rus', 8 => 'jpn');
	    foreach($out as $v){
	    	foreach($langs as $lang){
		        $name['locale'] = $lang;
		        $name['model'] = 'Menu';
		        $name['foreign_key'] = $v['Menu']['id'];
		        $name['field'] = 'name';
		        $name['content'] = $v['Menu']['name'];
		        $this->Traduc->query("INSERT INTO `i18n`(`locale`, `model`, `foreign_key`, `field`, `content`) VALUES ('".$name['locale']."', '".$name['model']."', '".$name['foreign_key']."', '".$name['field']."', '".$name['content']."')");

		        $url['locale'] = $lang;
		        $url['model'] = 'Menu';
		        $url['foreign_key'] = $v['Menu']['id'];
		        $url['field'] = 'url';
		        $url['content'] = $v['Menu']['url'];
		        $this->Traduc->query("INSERT INTO `i18n`(`locale`, `model`, `foreign_key`, `field`, `content`) VALUES ('".$url['locale']."', '".$url['model']."', '".$url['foreign_key']."', '".$url['field']."', '".$url['content']."')");
		        
		    }
	    }
	    $this->redirect('/admin/menus/');
	}
}
