<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('AppController', 'Controller');
App::uses('Paypal', 'Paypal.Lib');


/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class PagesController extends AppController {

/**
 * This controller does not use a model
 *
 * @var array
 */
	public $uses = array('Menu', 'Social', 'Content', 'Event', 'Artist','Order', 'Item', 'Type', 'Entry');
	public $theme = "Cardamomo";
	public $helper = "Bonzzay";
	public $components = array('Cookie','Session', 'RequestHandler');

	public function beforeFilter() {
		//setLocale(LC_ALL, "es_ES");
		$this->_setLanguage();

		//seo
		$this->set('title_for_layout', 'Cardamomo');
		$this->set('description_for_layout', 'Cardamomo, tablao flamenco');
		$this->set('keywords_for_layout', 'Tablo flamenco, flamenco madrid, tablo madrid');

		
	}
	function _setLanguage() {  

       
		if(isset($this->params['url']['lang'])){
			$this->Session->write('Lang.idioma', $this->params['url']['lang']);

		}
		if(!$this->Session->check('Lang.idioma')){
			$this->Session->write('Lang.idioma', 'esp');
		}
		
    } 

	public function beforeRender(){
		
		$this->set('idiomalang', $this->Session->read('Lang.idioma'));
		$this->Menu->locale = $this->Session->read('Lang.idioma');
		//top menu
		$topmenu = $this->Menu->find('all', array('conditions'=> array('Menu.position' => 1), 'order' => array('Menu.sort_order ASC')));
		
		$this->set('topmenus',$topmenu) ;
		


		//Footer
		$topfooter = $this->Menu->find('all', array('conditions'=> array('Menu.position' => 2), 'order' => array('Menu.sort_order ASC')));
		$this->set('topfooter',$topfooter) ;

		//subfooter
		$subfooter = $this->Menu->find('all', array('conditions'=> array('Menu.position' => 3), 'order' => array('Menu.sort_order ASC')));
		$this->set('subfooter',$subfooter) ;


		//render social
		$this->Social->locale = $this->Session->read('Lang.idioma');
		//top menu
		$this->set('socials', $this->Social->find('all', array('order' => array('Social.sort_order ASC'))));
		

		
	}


/**
 * Displays a view
 *
 * @return void
 * @throws NotFoundException When the view file could not be found
 *	or MissingViewException in debug mode.
 */
	public function display() {
		$path = func_get_args();

		$count = count($path);
		if (!$count) {
			return $this->redirect('/');
		}
		$page = $subpage = $title_for_layout = null;

		if (!empty($path[0])) {
			$page = $path[0];
		}
		if (!empty($path[1])) {
			$subpage = $path[1];
		}
		if (!empty($path[$count - 1])) {
			$title_for_layout = Inflector::humanize($path[$count - 1]);
		}
		$this->set(compact('page', 'subpage', 'title_for_layout'));

		try {
			$this->render(implode('/', $path));
		} catch (MissingViewException $e) {
			if (Configure::read('debug')) {
				throw $e;
			}
			throw new NotFoundException();
		}
	}
	public function home(){
		$this->layout = 'home';
		//subfooter
		$this->Menu->locale = $this->Session->read('Lang.idioma');

		$menuhome = $this->Menu->find('all', array('conditions'=> array('Menu.position' => 5), 'order' => array('Menu.sort_order ASC')));
		$this->set('menuhome',$menuhome);
		$month =  date("m");
		$year =  date('Y');
	
		//events month
		$eventsmonth = $this->Type->find('all', array('conditions'=> array('MONTH(Type.month)' => $month, 'YEAR(Type.month)' => $year), 'order' => array('Type.start ASC')));
		
		$this->set('eventsmonth',$eventsmonth);



	}
	public function content($slug=null){


		$this->Content->locale = $this->Session->read('Lang.idioma');
		$this->Content->recursive = 1;
		
		$content =  $this->Content->find('first', array('conditions' => array('I18n__slugTranslation.content' => $slug)));
		if($content == null){
			$this->redirect('/');
		}
		$this->set('content', $content);
	}

	public function artists(){
		$this->Artist->locale = $this->Session->read('Lang.idioma');
		$this->Artist->recursive = 2;
		$artists = $this->Artist->find('all', array('conditions'=>array('Artist.isactive' => true), 'order' => 'Artist.sort_order ASC'));
		$this->set('artists', $artists);
		$typeartists = $this->Artist->typeArtists();
		$this->set('typeartists', $typeartists);
	}
	public function artist($slug=null){
		$this->Artist->locale = $this->Session->read('Lang.idioma');
		$this->Artist->recursive = 2;
		$artist = $this->Artist->find('first', array('conditions' =>array('Artist.slug'=>$slug,'Artist.isactive' => true )));
		$this->set('artist', $artist);
		$typeartists = $this->Artist->typeArtists();
		$this->set('typeartists', $typeartists);
	}

	public function calendar($slug=null ){
		
		if($slug == __('cena-mas-espectaculo')){
			$this->Session->write('Tipo.num',2);
		}else if($slug == __('solocena')){
			$this->Session->write('Tipo.num',1);
		}else{
			$this->Session->write('Tipo.num',0);
		}

	}

	public function images(){
		$this->render('gallery');
	}
	public function galleries(){
		$this->Menu->locale = $this->Session->read('Lang.idioma');
		$this->layout = 'home';
		$menusgallery = $this->Menu->find('all', array('conditions'=> array('Menu.position' => 4), 'order' => array('Menu.sort_order ASC')));
		$this->set('menusgallery',$menusgallery) ;
	}

	public function localizacion(){
		$this->layout = 'contacto';
		if ($this->request->is('post') || $this->request->is('put')) {
			if(
				$this->request->data['Contact']['name'] == '' or
				$this->request->data['Contact']['email'] == '' or
				$this->request->data['Contact']['message'] == ''

			){
				// si los campos están vacios
				$this->Session->setFlash(__("Los campos no pueden estar vacios.", null));
				$message = __("Los campos no pueden estar vacios.");
				$this->redirect($this->referer());
			}

		}
		$this->Content->locale = $this->Session->read('Lang.idioma');
		$this->Content->recursive = -2;
		$content = $this->Content->find('first', array('conditions' =>array('Content.slug'=>__('localizacion-contacto'), 'Content.isactive'=>1 )));
		$this->set('content', $content);
	}

	public function event($id = null, $tipo = null){
		if($id != null){
			$this->Event->recursive = 2;
			
			if (!$this->Event->exists($id)) {
				throw new NotFoundException(__('Invalid type'));
			}
			$this->set('artists', $this->Artist->typeArtists());
			$options = array('conditions' => array('Event.' . $this->Event->primaryKey => $id));
			$this->set('event', $this->Event->find('first', $options));
			$this->set('tipo',$tipo);
		}else{
			
			$fecha = $this->request->data['Cal']['date'];
			$hora = $this->request->data['Cal']['dateevent'];
			$date = str_replace("/", "-", $fecha);
			$date = strtotime($date);
			$date =  date("Y-m-d",$date);
			$evento = null;

			$this->set('artists', $this->Artist->typeArtists());
			$this->Event->recursive = 2;
			$options = array('conditions' => array('DATE(Event.start)' => $date));
			$events = $this->Event->find('all', $options);
			if($events == null){
				$eventos = null;

			}else{
				$eventos = $events;

			}
			

			$u = 0;
			$isdinner= null;
			$hora = $hora.':00';
		
			foreach($events as $event){
			
				
					foreach($event['Type']['Entry'] as $entry){
						
						if($entry['start'] == $hora){
							$isdinner = $entry['isdinner'];
							$evento = $events[$u];
						}
					}
				

				$u++;
			}
			//debug($evento);
			$this->set('event', $evento);
			$this->set('tipo', $isdinner);
		}
		
		
	}
	public function day($fecha = null){
		$this->Event->recursive = 2;
		$events = $this->Event->find('all', array('conditions'=> array('DATE(Event.start)'=>$fecha)));
		$this->set('artists', $this->Artist->typeArtists());
		$this->set('events', $events);
		$this->set('datenow', $fecha);
		$this->set('tipo',$this->Session->read('Tipo.num'));
	}

	// The feed action is called from "webroot/js/ready.js" to get the list of events (JSON)
	public function feed($id=null) {
		$this->layout = "ajax";
		$data =array();
		/*$vars = $this->params['url'];
		$conditions = array('conditions' => array('Event.start >=' => $vars['start'], 'Event.start <=' => $vars['end']));
		$events = $this->Event->find('all',$conditions);
		//var_dump($conditions);
		foreach($events as $event) {
			if($event['Event']['all_day'] == 1) {
				$allday = true;
				$end = $event['Event']['start'];
			} else {
				$allday = false;
				$end = $event['Event']['end'];
			}
			$data[] = array(
					'id' => $event['Event']['id'],
					'title'=>$event['Event']['title'],
					'start'=>$event['Event']['start'],
					'end' => $end,
					'allDay' => $allday,
					'url' => Router::url('/') . 'event/'.$event['Event']['id'],
					'details' => $event['Event']['details'],
					'className' => $event['Type']['color']
			);
		}
		$this->set("json", json_encode($data));*/
		$this->set("json", json_encode($data));
	}

	public function eventshome(){
		if($this->RequestHandler->isAjax() || $this->request->ext == 'json') {
			$date  = $this->request->data['Event']['id'];
			//$date   = "2016-01-01";
			$date = str_replace("/", "-", $date);
			$date = strtotime($date);
			
			$month =  date("m",$date);
			$year =  date('Y',$date);
		
	
			//events month
			$this->Entry->recursive = 1;
			$this->Entry->locale = $this->Session->read('Lang.idioma');
			
			$eventsdinner= $this->Entry->find(
				'all', 
				array(
					'group'=> array('Entry.start'), 
					'conditions'=> array(
						'MONTH(Entry.month)' => $month, 
						'YEAR(Entry.month)' => $year,
						'Entry.isdinner' => 1
					)
				)
			);
			$events= $this->Entry->find(
				'all', 
				array(
					'group'=> array('Entry.start'), 
					'conditions'=> array(
						'MONTH(Entry.month)' => $month, 
						'YEAR(Entry.month)' => $year,
						'Entry.isdinner' => 0
					)
				)
			);

			$this->set('events' , $events);
			$this->set('eventsdinner' , $eventsdinner);
			$this->set('_serialize', array('events', 'eventsdinner'));
		}
	}

	public function order(){
		//debug($this->request->data);
		//comprobar datos del formulario
			
		if(
			$this->request->data['Order']['name'] == '' or
			$this->request->data['Order']['email'] == '' or
			$this->request->data['Order']['phone'] == ''

		){
			// si los campos están vacios
			$this->Session->setFlash(__("Los campos no pueden estar vacios.", null));
			$message = __("Los campos no pueden estar vacios.");
			$this->redirect($this->referer());
			

		}else if($this->request->data['Order']['email'] != $this->request->data['Order']['emailr']){
			// si los email no son iguales
			$this->Session->setFlash(__("Los email no coinciden.", null));
			$message = __("Los email no coinciden.");
			$this->redirect($this->referer());
		}
		


			
			

		// create order
		

		if ($this->request->is('post')) {
			$this->Order->create();
			$this->request->data['Order']['status']= 1;
			$this->request->data['Order']['no_publi']= $this->request->data['Order']['newsletter'];

			if ($this->Order->save($this->request->data)) {
				$lastid = $this->Order->getLastInsertID();
				$total = 0;
				$total_q = 0;
				$items = array();
				foreach($this->request->data['Item'] as $id=>$item){
					if($item['num'] != 0){
						$total = $total + ($item['num']*$item['price']);
						$total_q = $total_q +$item['num'];
						$data['Item']['order_id']=$lastid;
						$data['Item']['event_id']=$this->request->data['Order']['event_id'];
						$data['Item']['entry_id']=$id;
						$data['Item']['quantity']=$item['num'];
						$data['Item']['price']=$item['price'];
						$this->Item->create();
						$this->Item->save($data);
					}
				}
				$this->Order->id = $lastid;
				$this->Order->saveField('total_price', $total);
				$this->Order->saveField('total_quantity', $total);

				//$this->Session->setFlash(__('The order has been saved'), 'flash/success');
				// go to paypal
				$this->paypalpay($lastid);
			} else {
				$this->Session->setFlash(__('The order could not be saved. Please, try again.'), 'flash/error');
			}
		}
		
		
	}

	//paypal

	public function paypalpay($orderid = null){
		/*$this->Paypal = new Paypal(array(
		    'sandboxMode' => false,
		    'nvpUsername' => 'info_api1.clickprinting.es',
		    'nvpPassword' => 'DVFX455NVULB63T3',
		    'nvpSignature' => 'AFcWxV21C7fd0v3bYYYRCpSSRl31A1lpde3ZZfgcSPirX1spHJtSMyYd'
		));*/
		
		//sandbox
		$this->Paypal = new Paypal(array(
		    'sandboxMode' => true,
		    'nvpUsername' => 'info-facilitator_api1.clickprinting.es',
		    'nvpPassword' => '794PGJ9CMAUGAAFM',
		    'nvpSignature' => 'AFcWxV21C7fd0v3bYYYRCpSSRl31AUe96kybJqCB.3NzmhY3bvlfaCgX',
		    'oAuthClientId' => 'AYhxza079_TUWA14lMGc-gAZBiAfgKanV4vKPHWxhxSGk8j7YBVjuNNSRMTyeSgL20XY0YSL1qC0_yzm',
    		'oAuthSecret' => 'EM64ygr5MCnRtgXPLweDAb24YKrxlC1xEXj8_r2ZJliccxO5VHXhi-rbS_-q-K0P4tYEsi9sKmGBuxYm',
		));

		$this->Order->recursive = 2;
		$order = $this->Order->find('first',array('conditions'=>array('Order.id'=>$orderid)));
		/*debug($order);
		exit();*/
		$u = 0;
		$products = array();
		$total = str_replace(",", ".", $order['Order']['total_price']);
		$total = round($total,2);
		
		$event_id= null;
		foreach($order['Item'] as $m){
			
			
			$iva = ($m['price'] *21)/100;
			$iva = str_replace(',', '.', $iva);
			$iva = round($iva,2);
			
			$products[$u]['name'] = $m['Entry']['title'];
			$products[$u]['description'] = date("d/m/Y", strtotime($m['Event']['start'])).' ('.date("H:i", strtotime($m['Event']['start'])).' '.date("H:i", strtotime($m['Event']['end'])).')';
			$products[$u]['tax'] = round($iva,2);
			$pri = sprintf("%.2F", $m['price']);
			$products[$u]['subtotal'] = round($pri,2);
			$products[$u]['qty'] = $m['quantity'];
			$u++;
			$event_id = $m['Event']['id'];
		}
		
		
		$orderpaypal = array(
		    'description' => 'Cardamomo',
		    'currency' => 'EUR',
		    'return' => 'http://www.cardamomo.trial-web.net/paypalnotification/ok/'.$event_id.'/'.$orderid,
		    'cancel' => 'http://www.cardamomo.trial-web.net/paypalnotification/error/'.$event_id,
		    'custom' => 'Cardamomo',
		    //'shipping' => round('0',2),//str_replace(",", ".", $item['Order']['total_price']),
		    'items' => $products,
		    
		);
		
		

		try {
		    $url = $this->Paypal->setExpressCheckout($orderpaypal);

		    $this->redirect($url);
		} catch (Exception $e) {
			
		    $e->getMessage();var_dump($e->getMessage());

		}  

		  
	}

	public function paypalnotification($type = 0,$event_id = 0, $idorden = 0){
		

		
		
		if($type == 'error'){
			$message = __("Cancelación y/o error del pago.");
			$this->redirect('/event/'.$event_id);
		}elseif($type == 'ok'){
			$event = $this->Event->find('first', array('conditions'=>array('Event.id'=>$event_id)));
			$order = $this->Order->find('first', array('conditions'=>array('Order.id'=>$idorden)));
			
			//change quantity
			$this->Event->id = $lastid;
			$this->Event->saveField('quantity_buy', $event['Event']['quantity_buy']+$order['Order']['total_quantity']);

			//change status
			$this->Order->id = $idorden;
			$this->Order->saveField('status', 2);

			$this->redirect('/event/'.$event_id);
		}

		
		


	}
}
