<?php
App::uses('AppController', 'Controller');
/**
 * Socials Controller
 *
 * @property Social $Social
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class SocialsController extends AppController {

/**
 * Components
 *
 * @var array
 */

	public $components = array('Paginator', 'Flash', 'Session', 'Auth');

	public function beforeFilter() {
		$this->Auth->Allow('login');
		$this->Auth->logoutRedirect = array('/admin/users/login');
		
		$this->Auth->authenticate = array(
		    AuthComponent::ALL => array('userModel' => 'User'),
		    'Form'=> array(
                'fields' => array('username' => 'email'),
		    'Basic'));
		$this->Auth->authError = "Please log in first in order to preform that action.";

		
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Social->recursive = 0;
		$this->set('socials', $this->Social->find('all',array(
			'order' => 'sort_order ASC'
			)
		));
	}

	public function reorder() {
		foreach ($this->data['Social'] as $key => $value) {
		$this->Social->id = $value;
		$this->Social->saveField("sort_order",$key + 1);
		}
		//$this->log(print_r($this->data,true));
		exit();
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Social->exists($id)) {
			throw new NotFoundException(__('Invalid social'));
		}
		$options = array('conditions' => array('Social.' . $this->Social->primaryKey => $id));
		$this->set('social', $this->Social->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Social->create();
			if ($this->Social->save($this->request->data)) {
				$this->Session->setFlash(__('The social has been saved'), 'flash/success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The social could not be saved. Please, try again.'), 'flash/error');
			}
		}
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
        $this->Social->id = $id;
		if (!$this->Social->exists($id)) {
			throw new NotFoundException(__('Invalid social'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Social->save($this->request->data)) {
				$this->Session->setFlash(__('The social has been saved'), 'flash/success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The social could not be saved. Please, try again.'), 'flash/error');
			}
		} else {
			$options = array('conditions' => array('Social.' . $this->Social->primaryKey => $id));
			$this->request->data = $this->Social->find('first', $options);
		}
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Social->id = $id;
		if (!$this->Social->exists()) {
			throw new NotFoundException(__('Invalid social'));
		}
		if ($this->Social->delete()) {
			$this->Session->setFlash(__('Social deleted'), 'flash/success');
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Social was not deleted'), 'flash/error');
		$this->redirect(array('action' => 'index'));
	}
}
