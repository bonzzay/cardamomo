<?php
App::uses('AppController', 'Controller');
/**
 * Types Controller
 *
 * @property Type $Type
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class TypesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Flash', 'Session', 'Auth');
	public $helpers = array('Wysiwyg.Wysiwyg' => array('editor' => 'Ck'));

	public function beforeFilter() {
		$this->Auth->Allow('login');
		$this->Auth->logoutRedirect = array('/admin/users/login');
		
		$this->Auth->authenticate = array(
		    AuthComponent::ALL => array('userModel' => 'User'),
		    'Form'=> array(
                'fields' => array('username' => 'email'),
		    'Basic'));
		$this->Auth->authError = "Please log in first in order to preform that action.";
		if(isset($this->params['url']['lang'])){
			$this->Session->write('Lang.idioma', $this->params['url']['lang']);

		}
		if(!$this->Session->check('Lang.idioma')){
			$this->Session->write('Lang.idioma', 'esp');
		}
		$this->Type->Artist->locale = $this->Session->read('Lang.idioma');
		$this->Type->Guitar->locale = $this->Session->read('Lang.idioma');
		$this->Type->Dance->locale = $this->Session->read('Lang.idioma');
		$this->Type->Entry->locale = $this->Session->read('Lang.idioma');
	}
	public function beforeRender() {
		
		$this->set('idioma', $this->Session->read('Lang.idioma'));
		
	}


/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Type->recursive = 0;
		$this->set('types', $this->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Type->exists($id)) {
			throw new NotFoundException(__('Invalid type'));
		}
		$options = array('conditions' => array('Type.' . $this->Type->primaryKey => $id));
		$this->set('type', $this->Type->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Type->create();
			if ($this->Type->save($this->request->data)) {
				$this->Session->setFlash(__('The type has been saved'), 'flash/success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The type could not be saved. Please, try again.'), 'flash/error');
			}
		}
		$artists = $this->Type->Artist->find('list');
		$guitars = $this->Type->Guitar->find('list');
		$dances = $this->Type->Dance->find('list');
		$entries = $this->Type->Entry->find('list');
		$this->set(compact('artists', 'entries', 'guitars', 'dances'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
        $this->Type->id = $id;
		if (!$this->Type->exists($id)) {
			throw new NotFoundException(__('Invalid type'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Type->save($this->request->data)) {
				$this->Session->setFlash(__('The type has been saved'), 'flash/success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The type could not be saved. Please, try again.'), 'flash/error');
			}
		} else {
			$options = array('conditions' => array('Type.' . $this->Type->primaryKey => $id));
			$this->request->data = $this->Type->find('first', $options);
		}
		$artists = $this->Type->Artist->find('list');
		$guitars = $this->Type->Guitar->find('list');
		$dances = $this->Type->Dance->find('list');
		$entries = $this->Type->Entry->find('list');
		$this->set(compact('artists', 'entries', 'guitars', 'dances'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Type->id = $id;
		if (!$this->Type->exists()) {
			throw new NotFoundException(__('Invalid type'));
		}
		if ($this->Type->delete()) {
			$this->Session->setFlash(__('Type deleted'), 'flash/success');
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Type was not deleted'), 'flash/error');
		$this->redirect(array('action' => 'index'));
	}
}
