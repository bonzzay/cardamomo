<?php
App::uses('AppModel', 'Model');
/**
 * Artist Model
 *
 */
class Artist extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';

	public $actsAs = array(
		'Sluggable.Sluggable' => array(
		    'label'=>'name',
		    'slug'=>'slug',
		    'separator'=>'-',
		    'overwrite'=>false   
		),
		'Translate' => array(
			'name'   => 'nameTranslation',
            'slug'    => 'slugTranslation',
            'small_description'   => 'small_descriptionTranslation',
            'description'   => 'descriptionTranslation',
            'seo_title'   => 'seo_titleTranslation',
            'seo_description'   => 'seo_descriptionTranslation',
            'seo_keys'   => 'seo_keysTranslation',
        ),
		'Upload.Upload' => array(
            'image' => array(
                'fields' => array(
                    'dir' => 'image_dir'
                ),
                'thumbnailSizes' => array(
                    'thumb' => '247x247',
                )
            ),
            'photo' => array(
                'fields' => array(
                    'dir' => 'photo_dir'
                ),
                'thumbnailSizes' => array(
                    'thumb' => '960x600',
                )
            )
        )
    );

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'name' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'type' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		)
	);

/**
 * hasAndBelongsToMany associations
 *
 * @var array
 */
	public $hasAndBelongsToMany = array(
		'Type' => array(
			'className' => 'Type',
			'joinTable' => 'types_artists',
			'foreignKey' => 'artist_id',
			'associationForeignKey' => 'type_id',
			'unique' => 'keepExisting',
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
		),
		'Guitar' => array(
			'className' => 'Type',
			'joinTable' => 'types_guitars',
			'foreignKey' => 'artist_id',
			'associationForeignKey' => 'type_id',
			'unique' => 'keepExisting',
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
		),
		'Dance' => array(
			'className' => 'Type',
			'joinTable' => 'types_dances',
			'foreignKey' => 'artist_id',
			'associationForeignKey' => 'type_id',
			'unique' => 'keepExisting',
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
		)
	);

	function typeArtists(){
		$options = array(
		    '1'=>__('Bailaora'),
		    '2'=>__('Bailaor'),
		    '3'=>__('Cantaora'),
		    '4'=>__('Cantaor'),
		    '5'=>__('Guitarrista'),
		    '10'=>__('Guitarrista y compositor'),
		    '6'=>__('Cantante/Guitarrista'),
		    '7'=>__('Bailarín'),
		    '8'=>__('Cantaora/Bailaora'),
		    '9'=>__('Bailaora/Coreógrafa'),

		    //luego más
		);
		return $options;
	}
}
