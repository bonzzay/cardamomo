<?php
App::uses('AppModel', 'Model');
/**
 * Content Model
 *
 * @property Galery $Galery
 * @property Slider $Slider
 */
class Content extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'content';

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'title';

	public $actsAs = array(
		'Sluggable.Sluggable' => array(
		    'label'=>'title',
		    'slug'=>'slug',
		    'separator'=>'-',
		    'overwrite'=>false   
		),
		'Translate' => array(
			'title'   => 'titleTranslation',
            'slug'    => 'slugTranslation',
            'text'   => 'textTranslation',
            'seo_title'   => 'seo_titleTranslation',
            'seo_description'   => 'seo_descriptionTranslation',
            'seo_keys'   => 'seo_keysTranslation',
        ),
    );

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'title' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'text' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	// The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Galery' => array(
			'className' => 'Galery',
			'foreignKey' => 'galery_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Slider' => array(
			'className' => 'Slider',
			'foreignKey' => 'slider_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

	function ver($id = null){
		$this->locale = CakeSession::read('Lang.idioma');

		
		return $this->find('first', array('conditions'=> array('Content.id' =>$id)));
	}
}
