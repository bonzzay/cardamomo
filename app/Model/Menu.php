<?php
App::uses('AppModel', 'Model');
/*App::uses('Model','Content');*/
App::import('Model','Content');

/**
 * Menu Model
 *
 * @property Entity $Entity
 */
class Menu extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';

	public $actsAs = array(
		'Upload.Upload' => array(
            'image' => array(
                'fields' => array(
                    'dir' => 'image_dir'
                ),
                'thumbnailSizes' => array(
                    'thumb' => '625x625',
                    'big' => '404x404',
                )
            )
        ),
		'Translate' => array(
			'name'   => 'nameTranslation',
            'url'    => 'urlTranslation',
        ),

    );



/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'name' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'sort_order' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		
		'position' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	function staticContent(){
		$options = array(
		    __('calendar/cena-mas-espectaculo')=>__('Cena + Espectáculo'),
		    __('calendar/solocena')=>__('Espectáculo'),
		    __('galeria-de-fotos')=>__('Gallería fotos Flickr'),
		    __('fotos-y-videos-portada')=>__('Gallería fotos y videos'),
			__('artists')=>__('Artistas'),
			__('localizacion-contacto')=>__('Localización'),


		   /* '3'=>__('Cantaora'),
		    '4'=>__('Cantaor'),
		    '5'=>__('Guitarrista'),
		    '6'=>__('Cantante/Guitarrista'),
		    '7'=>__('Bailarín'),
		    '8'=>__('Cantaora/Bailaora'),
		    '9'=>__('Bailaora/Coreógrafa'),
		    '10'=>__('Guitarrista y compositor'),*/

		    //luego más
		);
		return $options;
	}

	function afterFind($results, $primary = false){
		
		$reut = array();
		$i = 0;
		foreach($results as $result){

			$model = $result['Menu']['model'];
			$foreign_key = $result['Menu']['foreign_key'];
			$url = $result['Menu']['url'];

			if($model != null){
				
		        //$this->ModelObject = Classregistry::init($model);
		        //$this->ModelObject = $this->loadModel($model);
		        
		        //var_dump($this->ModelObject->find('first'));
		        //$this->ModelObject->bindTranslation(array('slug' => 'slugTranslation'));
		        
		        //App::uses('Model',$model);
				//App::uses('Content');
				/*App::import('Model','Content');
				$anotherModel = new AnotherModel();
				$anotherModel->save($data);*/
				// if (App::import('Model', 'Content')) {
				//    $this->Content = new Content();
				// }

				$this->Content = Classregistry::init('Content');

				
		        $this->Content->locale = CakeSession::read('Lang.idioma');
		        $content = $this->Content->ver($foreign_key);
		  
		        /*$content = $this->Content->ver('first', array('conditions'=> array('Content.id' =>$foreign_key)));*/
		        $result['Page']['slug'] = $content['Content']['slug'];
		        $reut[$i] = $result;
			}else{
				$result['Page']['slug'] = $url;
		        $reut[$i] = $result;
			}
			
	        $i++;



		}
		//var_dump($reut);
		return $reut;
		//$this->contain($result['Menu']['model']);
   		/*return  $this->find('all', array());


	        $foreign_key = $this->data['Comment']['foreign_key'];
	        $model = $this->data['Comment']['model'];
	        
	        //TODO: class register con el $model
	        ClassRegistry::init($model);
	        $this->ModelObject = Classregistry::init($model);
	        //App::import('Model',$model);
	        $item = $this->ModelObject->read(null,$foreign_key);
	       if(isset($item[$model]['comments'])==true){
	           // $count = $item[$model]['comments'] + 1;
	           // $item[$model]['comments'] = $count;
	           // $this->$model->save($item);
	            $this->ModelObject->saveField('comments',$item[$model]['comments'] + 1);
	        }
	        return true;*/
	    }


}
