<?php
App::uses('AppModel', 'Model');
/**
 * Type Model
 *
 * @property Event $Event
 * @property Artist $Artist
 * @property Entry $Entry
 */
class Type extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';

	public $actsAs = array(
		
		/*'Translate' => array(
			'name'   => 'nameTranslation',
            'slug'    => 'slugTranslation',
            'small_description'   => 'small_descriptionTranslation',
            'description'   => 'descriptionTranslation',
            'seo_title'   => 'seo_titleTranslation',
            'seo_description'   => 'seo_descriptionTranslation',
            'seo_keys'   => 'seo_keysTranslation',
        ),*/
		'Upload.Upload' => array(
            'image' => array(
                'fields' => array(
                    'dir' => 'image_dir'
                ),
                'thumbnailSizes' => array(
                    'thumb' => '247x247',
                    'home' => '68x68',
                )
            )
        )
    );

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'name' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'color' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'start' => array(
			'time' => array(
				'rule' => array('time'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'num_artists' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'price' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	// The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	/*public $hasMany = array(
		'Event' => array(
			'className' => 'Event',
			'foreignKey' => 'type_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);*/


/**
 * hasAndBelongsToMany associations
 *
 * @var array
 */
	public $hasAndBelongsToMany = array(
		'Artist' => array(
			'className' => 'Artist',
			'joinTable' => 'types_artists',
			'foreignKey' => 'type_id',
			'associationForeignKey' => 'artist_id',
			'unique' => 'keepExisting',
			'conditions' => '',
			'fields' => '',
			'order' => 'type asc',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
		),
		'Guitar' => array(
			'className' => 'Artist',
			'joinTable' => 'types_guitars',
			'foreignKey' => 'type_id',
			'associationForeignKey' => 'artist_id',
			'unique' => 'keepExisting',
			'conditions' => '',
			'fields' => '',
			'order' => 'type asc',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
		),
		'Dance' => array(
			'className' => 'Artist',
			'joinTable' => 'types_dances',
			'foreignKey' => 'type_id',
			'associationForeignKey' => 'artist_id',
			'unique' => 'keepExisting',
			'conditions' => '',
			'fields' => '',
			'order' => 'type asc',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
		),
		'Entry' => array(
			'className' => 'Entry',
			'joinTable' => 'types_entries',
			'foreignKey' => 'type_id',
			'associationForeignKey' => 'entry_id',
			'unique' => 'keepExisting',
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
		)
	);

}
