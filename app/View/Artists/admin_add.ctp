<div id="content" class="row">
	<ul class="breadcrumb">
		<li><a href="<?php echo $this->Html->url('/'); ?>" class="glyphicons home"><i></i> Home</a></li>
		<li class="divider"></li>
		<li><?php echo __('Nuevo Artista'); ?></li>
	</ul>
	<div class="separator bottom"></div>
<!-- // Breadcrumb END -->

	<h3><?php echo __('Nuevo Artista'); ?></h3>

	
		<div class="innerLR">

		<?php echo $this->Form->create('Artist', array('role' => 'form', 'class' => 'form-horizontal', 'enctype'=>'multipart/form-data')); ?>
	
		<!--form class="form-horizontal" style="margin-bottom: 0;" id="validateSubmitForm" method="get" autocomplete="off" novalidate="novalidate"-->
		
		<!-- Widget -->
			<div class="widget">
			
				<!-- Widget heading -->
				<div class="widget-head">
					<h4 class="heading"><?php echo __('Nuevo Artista'); ?></h4>
				</div>
								<!-- // Widget heading END -->
				
								<div class="widget-body row">
									<div class="col-md-12">
															<div class="form-group">
						<?php echo $this->Form->input('name', array('class' => 'form-control','label' => 'Nombre del artista')); ?>
					</div><!-- .form-group -->
					<div class="form-group">
						<?php echo $this->Form->input('type', array('class' => 'selectpicker col-md-12','type' => 'select', 'options' => $typeartists, 'label'=> __('Tipo'))); ?>
					</div><!-- .form-group -->
					<div class="widget-body left">
						<label>Artista activo?</label>
						<div class="make-switch" data-on="default" data-off="default">
						
						<input type="checkbox" name="data[Artist][isactive]" value="1">
						</div>
					</div><!-- .form-group -->
					<div class="form-group">
						<?php echo $this->Form->input('small_description', array('class' => 'form-control','label' => 'Descripción corta')); ?>
					</div><!-- .form-group -->
					<div class="form-group">
						<? 
							$this->Wysiwyg->changeEditor('Ck');
							?>
							<label>Descripción</label>
							<? echo $this->Wysiwyg->textarea('Artist.description');?>
						
					</div><!-- .form-group -->
					
					<div class="form-group">
						<?php echo $this->Form->input('image', array('class' => 'form-control','type' => 'file', 'label' => 'Imagen pequeña listado')); ?>
					</div><!-- .form-group -->
					<div class="form-group">
						<?php echo $this->Form->input('photo', array('class' => 'form-control','type' => 'file', 'label' => 'Imagen grande detalle')); ?>
					</div><!-- .form-group -->
					<div class="form-group">
						<?php echo $this->Form->input('url_video', array('class' => 'form-control','label' => 'Url del video')); ?>
					</div><!-- .form-group -->
					<div class="form-group">
						<?php echo $this->Form->input('url_web', array('class' => 'form-control','label' => 'Url web')); ?>
					</div><!-- .form-group -->
					<div class="form-group">
						<?php echo $this->Form->input('seo_title', array('class' => 'form-control','label' => '(SEO) Título')); ?>
					</div><!-- .form-group -->
					<div class="form-group">
						<?php echo $this->Form->input('seo_description', array('class' => 'form-control','label' => '(SEO) Descripción')); ?>
					</div><!-- .form-group -->
					<div class="form-group">
						<?php echo $this->Form->input('seo_keys', array('class' => 'form-control','label' => '(SEO) Keys')); ?>
					</div><!-- .form-group -->

									</div>
										
									<!-- // Row END -->
									
										
								</div>
								<!-- // Row END -->
				
					<hr class="separator">
					
					<!-- Form actions -->
					<div class="form-actions">

						<button type="submit" class="btn btn-icon btn-primary glyphicons circle_ok"><i></i>Save</button>
						
					</div>
					<!-- // Form actions END -->
					
			</div>
		</div>
		<!-- // Widget END -->
		
				<?php echo $this->Form->end(); ?>
			
	</div><!-- /#page-content .col-sm-9 -->