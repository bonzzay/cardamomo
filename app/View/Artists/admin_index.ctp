
<div id="content" class="row">
<ul class="breadcrumb">
	<li><a href="<?php echo $this->Html->url('/'); ?>" class="glyphicons home"><i></i> Home</a></li>
	<li class="divider"></li>
	<li><?php echo __('Artistas'); ?></li>
</ul>
<div class="separator bottom"></div>
<!-- // Breadcrumb END -->

<h3><?php echo __('Artistas'); ?></h3>
	<div class="innerLR">
		<div class="widget">
		
			<div class="widget-head">
				<h4 class="heading"><?php echo __('Artistas'); ?></h4>
			</div>
			<div class="widget-timeline">
			<br>
				<ul id="my-list-artist" class="list-timeline">
				<?php foreach ($artists as $artist): ?>
				<li id='Artist_<?php echo $artist['Artist']['id']?>'> <span class="ellipsis"><?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $artist['Artist']['id']), array('class' => 'btn btn-default btn-xs')); ?></span> <span class="ellipsis"><?php echo $this->Form->postLink(__('Eliminar'), array('action' => 'delete', $artist['Artist']['id']), array('class' => 'btn btn-default btn-xs'), __('Estás seguro de borrar # %s?', $artist['Artist']['id'])); ?></span> <span class="glyphicons activity-icon move" style="cursor:move"><i></i></span>  <span>	<?php echo $artist['Artist']['name']; ?></span> </li>
				<?php endforeach; ?>
				</ul>
				
			</div><!-- /.table-responsive -->
			
		</div><!-- /.index -->


			
	
	</div><!-- /#page-content .col-sm-9 -->

</div><!-- /#content .row-fluid -->