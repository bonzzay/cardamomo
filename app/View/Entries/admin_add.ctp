<div id="content" class="row">
	<ul class="breadcrumb">
		<li><a href="<?php echo $this->Html->url('/'); ?>" class="glyphicons home"><i></i> Home</a></li>
		<li class="divider"></li>
		<li><?php echo __('Nueva entrada'); ?></li>
	</ul>
	<div class="separator bottom"></div>
<!-- // Breadcrumb END -->

	<h3><?php echo __('Nueva entrada'); ?></h3>

	
		<div class="innerLR">

		<?php echo $this->Form->create('Entry', array('role' => 'form', 'class' => 'form-horizontal')); ?>
	
		<!--form class="form-horizontal" style="margin-bottom: 0;" id="validateSubmitForm" method="get" autocomplete="off" novalidate="novalidate"-->
		
		<!-- Widget -->
			<div class="widget">
			
				<!-- Widget heading -->
				<div class="widget-head">
					<h4 class="heading"><?php echo __('Nueva entrada'); ?></h4>
				</div>
								<!-- // Widget heading END -->
				
								<div class="widget-body row">
									<div class="col-md-12">
					<div class="form-group">
						<?php echo $this->Form->input('name', array('class' => 'form-control','label' => 'Nombre')); ?>
					</div><!-- .form-group -->
					<div class="form-group">
						<?php echo $this->Form->input('title', array('class' => 'form-control','label' => 'Título')); ?>
					</div><!-- .form-group -->
					<div class="form-group">
						<? 
							$this->Wysiwyg->changeEditor('Ck');
						?>
						<label>Texto</label>
						<? echo $this->Wysiwyg->textarea('Entry.description');?>
					</div><!-- .form-group -->
					<div class="form-group">
						<?php echo $this->Form->input('price', array('class' => 'form-control','label' => 'Precio')); ?>
					</div><!-- .form-group -->
					<div class="form-group">
						<?php echo $this->Form->input('start', array('class' => 'form-control col-md-3','label' => 'Empieza')); ?>
					</div><!-- .form-group -->
					<div class="form-group">
						<label>Mes</label>
						
						<input size="16" type="text" class="form-control" name="data[ENtry][month]"  id="datetimepicker11"/>
						    
					    
					</div>
					
					<div class="widget-body left">
						<label>Es cena?</label>
						<div class="make-switch" data-on="default" data-off="default">
						
						<input type="checkbox" name="data[Entry][isdinner]" value="1">
						</div>
					</div><!-- .form-group -->
					
					
									</div>
										
									<!-- // Row END -->
									
										
								</div>
								<!-- // Row END -->
				
					<hr class="separator">
					
					<!-- Form actions -->
					<div class="form-actions">

						<button type="submit" class="btn btn-icon btn-primary glyphicons circle_ok"><i></i>Save</button>
						
					</div>
					<!-- // Form actions END -->
					
			</div>
		</div>
		<!-- // Widget END -->
		
				<?php echo $this->Form->end(); ?>
			
	</div><!-- /#page-content .col-sm-9 -->