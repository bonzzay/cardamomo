
<div id="content" class="row">
<ul class="breadcrumb">
	<li><a href="<?php echo $this->Html->url('/'); ?>" class="glyphicons home"><i></i> Home</a></li>
	<li class="divider"></li>
	<li><?php echo __('Entradas'); ?></li>
</ul>
<div class="separator bottom"></div>
<!-- // Breadcrumb END -->

<h3><?php echo __('Entradas'); ?></h3>
	<div class="innerLR">
		<div class="widget">
		
			<div class="widget-head">
				<h4 class="heading"><?php echo __('Entradas'); ?></h4>
			</div>
			<div class="widget-body">
				<table cellpadding="0" cellspacing="0" class="dynamicTable colVis table table-striped table-bordered table-condensed table-white dataTable">
					<thead>
						<tr>
							<th><?php echo $this->Paginator->sort('id'); ?></th>
							<th><?php echo $this->Paginator->sort('name', 'Nombre'); ?></th>
							<th><?php echo $this->Paginator->sort('title' ,'Título'); ?></th>
	
							<th><?php echo $this->Paginator->sort('price' ,'Precio'); ?></th>
							<th><?php echo $this->Paginator->sort('title' ,'Es cena?'); ?></th>
							<th><?php echo $this->Paginator->sort('created', 'Creado'); ?></th>
							<th><?php echo $this->Paginator->sort('modified', 'Modificado'); ?></th>
							<th class="actions"><?php echo __('Acciones'); ?></th>
						</tr>
					</thead>
					<tbody>
<?php foreach ($entries as $entry): ?>
	<tr>
		<td><?php echo h($entry['Entry']['id']); ?>&nbsp;</td>
		<td><?php echo h($entry['Entry']['name']); ?>&nbsp;</td>
		<td><?php echo h($entry['Entry']['title']); ?>&nbsp;</td>

		<td><?php echo h($entry['Entry']['price']); ?>&nbsp;</td>
		<td><?php if($entry['Entry']['isdinner'] == 1){
			echo 'Sí';
		}else{
			echo 'No';
			}; ?>&nbsp;</td>
		<td><?php echo h($entry['Entry']['created']); ?>&nbsp;</td>
		<td><?php echo h($entry['Entry']['modified']); ?>&nbsp;</td>
		<td class="actions">
			
			<?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $entry['Entry']['id']), array('class' => 'btn btn-default btn-xs')); ?>
			<?php echo $this->Form->postLink(__('Eliminar'), array('action' => 'delete', $entry['Entry']['id']), array('class' => 'btn btn-default btn-xs'), __('Are you sure you want to delete # %s?', $entry['Entry']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
					</tbody>
				</table>
			</div><!-- /.table-responsive -->
			<div class="row">
				<div class="col-md-11">
					<div class="dataTables_paginate paging_bootstrap">
						<ul class="pagination">
							<?php
					echo $this->Paginator->prev('< ' . __('Anterior'), array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
					echo $this->Paginator->numbers(array('separator' => '', 'currentTag' => 'a', 'tag' => 'li', 'currentClass' => 'disabled'));
					echo $this->Paginator->next(__('Siguiente') . ' >', array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
				?>
						</ul><!-- /.pagination -->
						
					</div>
				</div>
			</div>
		</div><!-- /.index -->
	
	</div><!-- /#page-content .col-sm-9 -->

</div><!-- /#content .row-fluid -->