<div id="content" class="row">
	<ul class="breadcrumb">
		<li><a href="<?php echo $this->Html->url('/'); ?>" class="glyphicons home"><i></i> Home</a></li>
		<li class="divider"></li>
		<li><?php echo __('Editar Evento'); ?></li>
	</ul>
	<div class="separator bottom"></div>
<!-- // Breadcrumb END -->

	<h3><?php echo __('Editar Evento'); ?></h3>

	
		<div class="innerLR">

		<?php echo $this->Form->create('Event', array('role' => 'form', 'class' => 'form-horizontal')); ?>
	
		<!--form class="form-horizontal" style="margin-bottom: 0;" id="validateSubmitForm" method="get" autocomplete="off" novalidate="novalidate"-->
		
		<!-- Widget -->
			<div class="widget">
			
				<!-- Widget heading -->
				<div class="widget-head">
					<h4 class="heading"><?php echo __('Editar página'); ?></h4>
				</div>
								<!-- // Widget heading END -->
				
					<div class="widget-body row">
						<div class="col-md-12">
						<? echo $this->Form->input('id');?>
					<div class="form-group">
						<?php echo $this->Form->input('type_id', array('class' => 'form-control', 'label' => 'Tipo de evento')); ?>
					</div><!-- .form-group -->

					
					<div class="form-group">
						<?php echo $this->Form->input('title', array('class' => 'form-control', 'label' => 'Título')); ?>
					</div><!-- .form-group -->
					
										<div class="form-group">
						<? 
							$this->Wysiwyg->changeEditor('Ck');
						?>
						<label>Detalles</label>
						<? echo $this->Wysiwyg->textarea('Event.details');?>
					</div><!-- .form-group -->
					<div class="form-group">
						<label>Empieza</label>
						
						<input size="16" type="text" class="form-control" name="data[Event][start]" value="<? echo $event['Event']['start'];?>"  id="datetimepicker1"/>
						    
					    
					</div>
					<div class="form-group">
						<label>Termina</label>
						
						<input size="16" type="text" class="form-control" name="data[Event][end]" value="<? echo $event['Event']['end'];?>"  id="datetimepicker2"/>
						    
					    
					</div>
					
					
					
					
					<div class="form-group">
						<?php echo $this->Form->input('quantity', array('class' => 'form-control', 'label' => 'Cantidad')); ?>
					</div><!-- .form-group -->

					

									</div>
										
									<!-- // Row END -->
									
										
								</div>
								<!-- // Row END -->
				
					<hr class="separator">
					
					<!-- Form actions -->
					<div class="form-actions">

						<button type="submit" class="btn btn-icon btn-primary glyphicons circle_ok"><i></i>Guardar</button>
						
					</div>
					<!-- // Form actions END -->
					
			</div>
		</div>
	<!-- // Widget END -->
	
			<?php echo $this->Form->end(); ?>
		
</div><!-- /#page-content .col-sm-9 -->

