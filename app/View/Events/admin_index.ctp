<div id="content" class="row">
<ul class="breadcrumb">
	<li><a href="<?php echo $this->Html->url('/'); ?>" class="glyphicons home"><i></i> Home</a></li>
	<li class="divider"></li>
	<li><?php echo __('Eventos'); ?></li>
</ul>
<div class="separator bottom"></div>
<!-- // Breadcrumb END -->

<h3><?php echo __('Eventos'); ?></h3>
<div class="innerLR">
		<div class="widget">
		
			<div class="widget-head">
				<h4 class="heading"><?php echo __('Eventos'); ?></h4>
			</div>
			<div class="widget-body">
			<?php echo $this->Form->create('Event',array('action'=>'deletes'));?>
				<?echo $this->Js->submit('borrar masivo', array(
					  'update' => '#mytable'
					));?>
				<table cellpadding="0" cellspacing="0" class="dynamicTable colVis table table-striped table-bordered table-condensed table-white dataTable">
					<thead>
						<tr>
							<th><?php echo $this->Paginator->sort('type_id', 'Tipo');?></th>
							<th><?php echo $this->Paginator->sort('title', 'Título');?></th>
							<th><?php echo $this->Paginator->sort('status', 'Estado');?></th>
							<th><?php echo $this->Paginator->sort('start', 'Comienza');?></th>
				            <th><?php echo $this->Paginator->sort('end', 'Termina');?></th>
				            <th><?php echo $this->Paginator->sort('all_day', 'Todo el día');?></th>
							<th class="actions"><?php echo __('Acciones'); ?></th>
						</tr>
					</thead>
					<tbody>
<?php foreach ($events as $event): ?>
	<tr>
		<td>
			<? echo $this->Form->checkbox('Event.'.$event['Event']['id'], array(
								  'value' => $event['Event']['id'],
								  'hiddenField' => false
								));?>
			<?php echo $this->Html->link($event['Type']['name'], array('controller' => 'types', 'action' => 'view', $event['Type']['id'])); ?>
		</td>
		<td><?php echo $event['Event']['title']; ?></td>
		<td><?php echo $event['Event']['status']; ?></td>
		<td><?php echo $event['Event']['start']; ?></td>
        <?php if($event['Event']['all_day'] == 0) { ?>
   		<td><?php echo $event['Event']['end']; ?></td>
        <?php } else { ?>
		<td>N/A</td>
        <?php } ?>
        <td><?php if($event['Event']['all_day'] == 1) { echo "Sí"; } else { echo "No"; } ?>&nbsp;</td>
		<td class="actions">
			
			<?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $event['Event']['id']), array('class' => 'btn btn-default btn-xs')); ?>
			<? if($event['Event']['active'] == 1):?>
				<?php echo $this->Html->link(__('Cerrar el evento'), array('action' => 'close', $event['Event']['id'],0), array('class' => 'btn btn-default btn-xs')); ?>
			<? else:?>
				<?php echo $this->Html->link(__('Abrir el evento'), array('action' => 'close', $event['Event']['id'],1), array('class' => 'btn btn-default btn-xs')); ?>
			<? endif;?>
			<?php echo $this->Form->postLink(__('Eliminar'), array('action' => 'delete', $event['Event']['id']), array('class' => 'btn btn-default btn-xs'), __('Seguro que quiere borrar # %s?', $event['Event']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
					</tbody>
				</table>
				<? echo $this->Form->end();?>

			</div><!-- /.table-responsive -->
			<div class="row">
				<div class="col-md-11">
					<div class="dataTables_paginate paging_bootstrap">
						<ul class="pagination">
							<?php
					echo $this->Paginator->prev('< ' . __('Anterior'), array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
					echo $this->Paginator->numbers(array('separator' => '', 'currentTag' => 'a', 'tag' => 'li', 'currentClass' => 'disabled'));
					echo $this->Paginator->next(__('Siguiente') . ' >', array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
				?>
						</ul><!-- /.pagination -->
						
					</div>
				</div>
			</div>
		</div><!-- /.index -->
	
	</div><!-- /#page-content .col-sm-9 -->

</div><!-- /#content .row-fluid -->