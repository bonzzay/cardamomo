
<div id="content" class="row">
<ul class="breadcrumb">
	<li><a href="<?php echo $this->Html->url('/'); ?>" class="glyphicons home"><i></i> Home</a></li>
	<li class="divider"></li>
	<li><?php echo __('Galerías'); ?></li>
</ul>
<div class="separator bottom"></div>
<!-- // Breadcrumb END -->

<h3><?php echo __('Galerías'); ?></h3>
	<div class="innerLR">
		<div class="widget">
		
			<div class="widget-head">
				<h4 class="heading"><?php echo __('Galerías'); ?></h4>
			</div>
			<div class="widget-body">
				<table cellpadding="0" cellspacing="0" class="dynamicTable colVis table table-striped table-bordered table-condensed table-white dataTable">
					<thead>
						<tr>
							<th><?php echo $this->Paginator->sort('id'); ?></th>
							<th><?php echo $this->Paginator->sort('name', 'nombre'); ?></th>
							<th><?php echo $this->Paginator->sort('created', 'Creado'); ?></th>
							<th><?php echo $this->Paginator->sort('modified', 'Modificado'); ?></th>
							<th class="actions"><?php echo __('Acciones'); ?></th>
						</tr>
					</thead>
					<tbody>
<?php foreach ($galeries as $galery): ?>
	<tr>
		<td><?php echo h($galery['Galery']['id']); ?>&nbsp;</td>
		<td><?php echo h($galery['Galery']['name']); ?>&nbsp;</td>
		<td><?php echo h($galery['Galery']['created']); ?>&nbsp;</td>
		<td><?php echo h($galery['Galery']['modified']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('Nueva Imagen'), array('controller' =>'images','action' => 'add', $galery['Galery']['id'],1), array('class' => 'btn btn-default btn-xs')); ?>
			<?php echo $this->Html->link(__('Ver'), array('action' => 'view', $galery['Galery']['id']), array('class' => 'btn btn-default btn-xs')); ?>
			<?php echo $this->Form->postLink(__('Eliminar'), array('action' => 'delete', $galery['Galery']['id']), array('class' => 'btn btn-default btn-xs'), __('Are you sure you want to delete # %s?', $galery['Galery']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
					</tbody>
				</table>
			</div><!-- /.table-responsive -->
			<div class="row">
				<div class="col-md-11">
					<div class="dataTables_paginate paging_bootstrap">
						<ul class="pagination">
							<?php
					echo $this->Paginator->prev('< ' . __('Anterior'), array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
					echo $this->Paginator->numbers(array('separator' => '', 'currentTag' => 'a', 'tag' => 'li', 'currentClass' => 'disabled'));
					echo $this->Paginator->next(__('Proximo') . ' >', array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
				?>
						</ul><!-- /.pagination -->
						
					</div>
				</div>
			</div>
		</div><!-- /.index -->
	
	</div><!-- /#page-content .col-sm-9 -->

</div><!-- /#content .row-fluid -->