<div id="content" class="row">
<ul class="breadcrumb">
	<li><a href="<?php echo $this->Html->url('/'); ?>" class="glyphicons home"><i></i> Home</a></li>
	<li class="divider"></li>
	<li><?php echo __('Galerías'); ?></li>
</ul>
<div class="separator bottom"></div>
<!-- // Breadcrumb END -->

<h3><?php  echo __('Galerías'); ?></h3>
	<div class="innerLR">
		<div class="widget">
		
			<div class="widget-head">
				<h4 class="heading"><?php echo $galery['Galery']['name']; ?></h4>
			</div>
			<div class="widget-body">
				<div class="row">
					<ul class="thumbnails">
					<? foreach($galery['Image'] as $image):?>
						<li class="col-md-3"><div class="thumbnail"><img src="<? echo $this->Html->url('/');?>files/image/image/<? echo $image['image_dir'];?>/thumb_<? echo $image['image'];?>" alt="<? echo $image['title'];?>" /> 
						<?php echo $this->Form->postLink(__('Eliminar'), array('controller' => 'images','action' => 'delete', $image['id'],1), array('class' => 'btn btn-default btn-xs'), __('Are you sure you want to delete # %s?', $image['id'])); ?>

						</div></li>
					<? endforeach;?>
						
					</ul>
				</div>
			</div><!-- /.table-responsive -->
	
	</div><!-- /#page-content .col-sm-9 -->

</div><!-- /#content .row-fluid -->
