<?
// This file is part of Questionity - http://www.questionity.com/
// 
/**
 * Questionity helpers.
 *                        
 *
 * @package    frontend
 * @subpackage app.views.helpers.questionity
 * @copyright  2011 Questionity
 */
 
 /**
  * Questionity helper class
  *
  */
class BonzzayHelper extends AppHelper {

  var $helpers = array('Html', 'Session'); 

     /**
      * videos function create video form url (youtube and vimeo)).
      *
      * @param string $url
      * @param string $title
      * @param sring $width
      * @param string $height
      * @return string
      */


     function videos($url, $title, $width = 350 ,$height = 285){
        $urlinfo  = $this->parse_urlv($url);
        $video =''; 
        if (!empty($urlinfo) && isset($urlinfo['host'])){
           $host = $urlinfo['host'];
           
           $video .= '<div class="mtop floatleft">';
           if (($host == 'www.youtube.com') ||
               ($host == 'youtube.com') || 
               ($host == 'youtu.be')) {
              //youtube
                 if( stripos($url , 'v=') != false ){
                      $v = explode("v=",$url);
                      if( stripos($v[1] , '&') === false ){
                         $v[0] =  $v[1];
                      }else{
                        $v = explode("&",$v[1]);
                      }
                      
                  }else{
                    $v = explode("be/",$url); 
                    $v[0] = $v[1];
                  }
                  $video .= ' <div>
                   <iframe title="'.$title.'" width="'.$width.'" height="'.($height+30).'" src="http://www.youtube.com/embed/'.$v[0].'?wmode=transparent" frameborder="0" allowfullscreen></iframe>
                   </div> ';

           } else if (($host == 'www.vimeo.com') ||
               ($host == 'vimeo.com')) {
               //vimeo
               $v = explode(".com/",$url); 
               $url = $v[1];
              $video .= '<iframe src="http://player.vimeo.com/video/'.$url.'?title=0" width="'.$width.'" height="'.($height + 20).'" frameborder="0"></iframe>';
              
           }
           
           $video .= '</div>';
        }
     return $video;
         
     }
     
     private function parse_urlv($url) {
       $urlinfo = @parse_url($url);
       $urlinfo['url'] = $url;
       if (isset($urlinfo['query'])) {
          $vars = explode('&', $urlinfo['query']);
          $variables = array();
          foreach ($vars as $var) {
             $var = explode('=',$var);
             $variables[$var[0]] = $var[1];
          }
          $urlinfo['variables'] = $variables;
       }
       
       return $urlinfo;
    }

    public function limita_caracteres($texto, $limite, $quebra = true) {
      $tamanho = strlen($texto);
      // Verifica se o tamanho do texto é menor ou igual ao limite
      if ($tamanho <= $limite) {
          $novo_texto = $texto;
      // Se o tamanho do texto for maior que o limite
      } else {
          // Verifica a opção de quebrar o texto
          if ($quebra == true) {
              $novo_texto = trim(substr($texto, 0, $limite)).'...';
          // Se não, corta $texto na última palavra antes do limite
          } else {
              // Localiza o útlimo espaço antes de $limite
              $ultimo_espaco = strrpos(substr($texto, 0, $limite), ' ');
              // Corta o $texto até a posição localizada
              $novo_texto = trim(substr($texto, 0, $ultimo_espaco)).'...';
          }
      }
      // Retorna o valor formatado
      return $novo_texto;
    }



}