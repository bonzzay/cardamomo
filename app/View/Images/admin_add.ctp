<div id="content" class="row">
	<ul class="breadcrumb">
		<li><a href="<?php echo $this->Html->url('/'); ?>" class="glyphicons home"><i></i> Home</a></li>
		<li class="divider"></li>
		<li><?php echo __('Añadir imagen'); ?></li>
	</ul>
	<div class="separator bottom"></div>
<!-- // Breadcrumb END -->

	<h3><?php echo __('Añadir imagen'); ?></h3>

	
		<div class="innerLR">

		<?php echo $this->Form->create('Image', array('role' => 'form', 'class' => 'form-horizontal','enctype'=>'multipart/form-data')); ?>
	
		<!--form class="form-horizontal" style="margin-bottom: 0;" id="validateSubmitForm" method="get" autocomplete="off" novalidate="novalidate"-->
		
		<!-- Widget -->
			<div class="widget">
			
				<!-- Widget heading -->
				<div class="widget-head">
					<h4 class="heading"><?php echo __('Añadir imagen'); ?></h4>
				</div>
								<!-- // Widget heading END -->
				
				<div class="widget-body row">
				<div class="col-md-12">
				<? if($type == 2):?>
					<div class="form-group">
						<?php echo $this->Form->input('slider_id', array('type' => 'hidden', 'value' => $id)); ?>
					</div><!-- .form-group -->
				<? else:?>
					<div class="form-group">
						<?php echo $this->Form->input('galery_id', array('type' => 'hidden', 'value' => $id)); ?>
					</div><!-- .form-group -->
				<? endif;?>
					<div class="form-group">
						<?php echo $this->Form->input('title', array('class' => 'form-control', 'label' => 'Título')); ?>
					</div><!-- .form-group -->
					<div class="form-group">
						<?php echo $this->Form->input('image', array('class' => 'form-control','type' => 'file', 'label' => 'Imagen')); ?>
					</div><!-- .form-group -->
					

									</div>
										
									<!-- // Row END -->
									
										
								</div>
								<!-- // Row END -->
				
					<hr class="separator">
					
					<!-- Form actions -->
					<div class="form-actions">

						<button type="submit" class="btn btn-icon btn-primary glyphicons circle_ok"><i></i>Save</button>
						
					</div>
					<!-- // Form actions END -->
					
			</div>
		</div>
		<!-- // Widget END -->
		
				<?php echo $this->Form->end(); ?>
			
	</div><!-- /#page-content .col-sm-9 -->