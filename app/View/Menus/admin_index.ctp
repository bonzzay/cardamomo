
<div id="content" class="row">
<ul class="breadcrumb">
	<li><a href="<?php echo $this->Html->url('/'); ?>" class="glyphicons home"><i></i> Home</a></li>
	<li class="divider"></li>
	<li><?php echo __('Menus'); ?></li>
</ul>
<div class="separator bottom"></div>
<!-- // Breadcrumb END -->

<h3><?php echo __('Menus'); ?></h3>

	<div class="innerLR">
		<div class="widget">
			
			<div class="widget-head">
				<h4 class="heading"><?php echo $menuname; ?></h4>
			</div>
			<div class="widget-body">
				<div class="widget-timeline">
				<br>
					<ul id="my-list-menu" class="list-timeline">
					<?php foreach ($menus as $menu): ?>
					<li id='Menu_<?php echo $menu['Menu']['id']?>'> <span class="ellipsis"><?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $menu['Menu']['id']), array('class' => 'btn btn-default btn-xs')); ?></span> <span class="ellipsis"><?php echo $this->Form->postLink(__('Eliminar'), array('action' => 'delete', $menu['Menu']['id'], $menu['Menu']['position']), array('class' => 'btn btn-default btn-xs'), __('Realmente desea borrarlo # %s?', $menu['Menu']['name'])); ?></span> <span class="glyphicons activity-icon move" style="cursor:move"><i></i></span>  <span>	<?php echo $menu['Menu']['name']; ?></span> </li>
					<?php endforeach; ?>
					</ul>
					
				</div><!-- /.table-responsive -->
				
			</div><!-- /.table-responsive -->
			
		</div><!-- /.index -->
	<?php echo $this->Html->link(__('Nuevo elemento'), array('action' => 'add', $position), array('class' => 'btn btn-default btn-xs')); ?>
	</div><!-- /#page-content .col-sm-9 -->

</div><!-- /#content .row-fluid -->