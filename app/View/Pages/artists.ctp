<h1 class="post-title"><? echo __("Artistas");?>
<a href=" https://www.flickr.com/photos/cardamomotablao/albums/72157636208422915" target="_blank"> 
		
		<? echo  $this->Html->image('/theme/Cardamomo/images/fotos.png', array('alt'=>'fotos', 'title'=>'fotos','width' => '48' , 'height' => '48'));?> 
</a>
</h1>
<hr class="thick">
<div class="entry-body">
	<div itemprop="articleBody">
	<? foreach($artists as $artist):?>

		<div class="artistContent">
			 <div class="su-column su-column-size-1-4 ArtistDetails leftContent">
			 	<div class="su-column-inner su-clearfix"> 
			 	<a class="wplightbox" href="<? echo $this->Html->url('/');?>artists/<? echo $artist['Artist']['slug'];?>"><img class="size-medium wp-image-208" src="<? echo $this->Html->url('/');?>files/artist/image/<? echo $artist['Artist']['image_dir'];?>/<? echo $artist['Artist']['image'];?>" alt="<? echo $artist['Artist']['name'];?>" width="247" height="247"></a>
			 		
			 		<div class="interestLinks">
			 			<a href="<? echo $this->Html->url('/');?>artists/<? echo $artist['Artist']['slug'];?>" class="wplightbox cardamomoButton">+ sobre el artista</a>
			 		</div>
			 	</div>
			 </div>
			 <div class="textContent">
				<div class="header_textContent">
					<h4><? echo $artist['Artist']['name'];?></h4>
					<? foreach($typeartists as $clave=>$valor):?>
					<? if($clave == $artist['Artist']['type']):?>
						<p><? echo $valor;?></p>

					<? endif;?>
					<? endforeach;?>
					
				</div>
				<? echo $artist['Artist']['small_description'];?>

			</div>
		</div>
		<div style="clear:both;"></div>
	<? endforeach;?>
	</div>
</div>