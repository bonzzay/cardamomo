<div itemprop="articleBody">

<h1 class="post-title"><? echo __("Espectáculos");?>  <? echo date("j F Y", strtotime($datenow)); ?></h1>
<? foreach($events as $event):?>
	
	<hr class="thick">
	<h3><? echo date("d/m/Y", strtotime($event['Event']['start']));?></h3>
	
	<div class="showEvent">
		<div class="left-link-event">
			
			<div class="events-table-link-left">
			<?
			$nuevafecha = strtotime ( '-45 minute' , strtotime ( $event['Event']['start'] ) ) ;
			$nuevafecha = date ( 'H:i' , $nuevafecha );
			?>
				<a class="link_button" href="<? echo $this->Html->url('/');?>event/<? echo $event['Event']['id'];?>/2"><? echo __("Reserva Cena+Espectáculo");?>  <? echo $nuevafecha;?></a>
			</div>

			<? if($tipo != 2):?>
				<div class="events-table-link-left">
					<a class="link_button" href="<? echo $this->Html->url('/');?>event/<? echo $event['Event']['id'];?>/1"><? echo __("Reserva Espectáculo");?> <? echo date("H:i", strtotime($event['Event']['start']));?></a>
				</div>
			<? endif;?>
		</div>
		
		<div class="show">
			<div class="sessionsShow">
				
				<p><b><? echo __("Nº de artistas en escena:");?></b> <? echo $event['Type']['num_artists'];?>.</p>
				<p><b><? echo __("Precio de la entrada:");?></b> <? echo $event['Type']['price'];?> €. <? echo __("Incluye 1 sangría de consumición.");?></p>
			</div>
			<ul class="EventFigurants large_figurants">
				<li>
				<? echo __("Baile:");?>
				<? $i = 0;?>
				<? $len = count($event['Type']['Dance']);?>
				<? foreach($event['Type']['Dance'] as $art):?>
					<? if($art['isactive'] == 1):?>
						<a href="<? echo $this->Html->url('/');?>artists/<? echo $art['slug'];?>"><? echo $art['name'];?></a>
					<? else:?>
						<? echo $art['name'];?>
					<? endif;?>
					
					<? 
					$cont = '';
					if ($i == $len - 1) {
				        // last
				        $cont='';
				    }else if ($i == $len - 2) {
				        // last
				        $cont=__(' y ');
				    }else{
				    	$cont=__(', ');
				    }
				    echo $cont;
					?>
					<? $i++;?>
				<? endforeach;?>
				</li>
				<li>
				<? echo __("Cante:");?>
				<? $i = 0;?>
				<? $len = count($event['Type']['Artist']);?>
				<? foreach($event['Type']['Artist'] as $art):?>
					<? if($art['isactive'] == 1):?>
						<a href="<? echo $this->Html->url('/');?>artists/<? echo $art['slug'];?>"><? echo $art['name'];?></a>
					<? else:?>
						<? echo $art['name'];?>
					<? endif;?>

					<? 
					$cont = '';
					if ($i == $len - 1) {
				        // last
				        $cont='';
				    }else if ($i == $len - 2) {
				        // last
				        $cont=__(' y ');
				    }else{
				    	$cont=__(', ');
				    }
				    echo $cont;
					?>
					<? $i++;?>
				<? endforeach;?>
				</li>
				<li>
				<? echo __("Guitarra:");?>
				<? $i = 0;?>
				<? $len = count($event['Type']['Guitar']);?>
				<? foreach($event['Type']['Guitar'] as $art):?>
					<? if($art['isactive'] == 1):?>
						<a href="<? echo $this->Html->url('/');?>artists/<? echo $art['slug'];?>"><? echo $art['name'];?></a>
					<? else:?>
						<? echo $art['name'];?>
					<? endif;?>

					<? 
					$cont = '';

					if ($i == $len - 1) {
				        // last
				        $cont='';
				    }else if ($i == $len - 2) {
				        // last
				        $cont=__(' y ');
				    }else{
				    	$cont=__(', ');
				    }
				    echo $cont;
					?>
					<? $i++;?>
				<? endforeach;?>
				</li>
			</ul>
		</div>
	</div>
<? endforeach;?>
	
</div>