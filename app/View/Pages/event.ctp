<? if($event == null):?>
<div itemprop="articleBody">
	<h1 class="post-title"><? echo __("No hay Espectáculos disponibles");?></h1>
	<hr class="thick">
</div>

<? else:?>
<div itemprop="articleBody">
	<h1 class="post-title"><? echo $event['Event']['title'];?></h1>
	<hr class="thick">
	<?
	if($tipo == 2){
		$isdinner = 1;
		$nuevafecha = strtotime ( '-45 minute' , strtotime ( $event['Event']['start'] ) ) ;
		$nuevafecha = date ( 'H:i' , $nuevafecha );
	}else{
		$nuevafecha = date("H:i", strtotime($event['Event']['start']));
		$isdinner = 0;
	}
	
	?>
	<h3><? echo date("d/m/Y", strtotime($event['Event']['start']));?> (<? echo $nuevafecha;?> - <? echo date("H:i", strtotime($event['Event']['end']));?>)</h3>
	<div class="showEvent">
		<div class="show">
			<div class="sessionsShow">
				<p><b><? echo __("Hora de inicio:");?></b> <? echo $nuevafecha;?> <? echo __("horas");?></p>
				<p><b><? echo __("Nº de artistas en escena:");?></b> <? echo $event['Type']['num_artists'];?>.</p>
				<p><b><? echo __("Precio de la entrada:");?></b> <? echo $event['Type']['price'];?> €. <? echo __("Incluye 1 sangría de consumición.");?></p>
			</div>
			<ul class="EventFigurants large_figurants">
				<li>
				<? echo __("Baile:");?>
				<? $i = 0;?>
				<? $len = count($event['Type']['Dance']);?>
				<? foreach($event['Type']['Dance'] as $art):?>
					<? if($art['isactive'] == 1):?>
						<a href="<? echo $this->Html->url('/');?>artists/<? echo $art['slug'];?>"><? echo $art['name'];?></a>
					<? else:?>
						<? echo $art['name'];?>
					<? endif;?>
					<? 
					$cont = '';
					if ($i == $len - 1) {
				        // last
				        $cont='';
				    }else if ($i == $len - 2) {
				        // last
				        $cont=__(' y ');
				    }else{
				    	$cont=__(', ');
				    }
				    echo $cont;
					?>
					<? $i++;?>
				<? endforeach;?>
				</li>
				<li>
				<? echo __("Cante:");?>
				<? $i = 0;?>
				<? $len = count($event['Type']['Artist']);?>
				<? foreach($event['Type']['Artist'] as $art):?>
					<? if($art['isactive'] == 1):?>
						<a href="<? echo $this->Html->url('/');?>artists/<? echo $art['slug'];?>"><? echo $art['name'];?></a>
					<? else:?>
						<? echo $art['name'];?>
					<? endif;?>
					<? 
					$cont = '';
					if ($i == $len - 1) {
				        // last
				        $cont='';
				    }else if ($i == $len - 2) {
				        // last
				        $cont=__(' y ');
				    }else{
				    	$cont=__(', ');
				    }
				    echo $cont;
					?>
					<? $i++;?>
				<? endforeach;?>
				</li>
				<li>
				<? echo __("Guitarra:");?>
				<? $i = 0;?>
				<? $len = count($event['Type']['Guitar']);?>
				<? foreach($event['Type']['Guitar'] as $art):?>
					<? if($art['isactive'] == 1):?>
						<a href="<? echo $this->Html->url('/');?>artists/<? echo $art['slug'];?>"><? echo $art['name'];?></a>
					<? else:?>
						<? echo $art['name'];?>
					<? endif;?>
					<? 
					$cont = '';

					if ($i == $len - 1) {
				        // last
				        $cont='';
				    }else if ($i == $len - 2) {
				        // last
				        $cont=__(' y ');
				    }else{
				    	$cont=__(', ');
				    }
				    echo $cont;
					?>
					<? $i++;?>
				<? endforeach;?>
				</li>
			</ul>
		</div>
	</div>
	<? if($event['Event']['active'] == 0):?>
		<div class="em-booking-message-error em-booking-message"><? echo __("Evento cerrado, probablemente porque quedan pocas entradas.");?></div>
	<? else:?>
	
	<h3><? echo __("Reserva");?></h3>
	
	<div id="em-booking" class="em-booking css-booking">
		
			<div class="em-booking-message-error em-booking-message"><? echo $this->Session->flash();?></div>
		
		<form class="em-booking-form" name="booking-form" method="post" action="<? echo $this->Html->url('/');?>order">
		<input type="hidden" name="data[Order][event_id]" value="<? echo $event['Event']['id'];?>">
			<table class="em-tickets" cellspacing="0" cellpadding="0">
				<tbody>
					<tr>
						<th class="em-bookings-ticket-table-type"><? echo __("Tipo de Entrada");?></th>
						<th class="em-bookings-ticket-table-price"><? echo __("Precio");?></th>
						<th class="em-bookings-ticket-table-spaces"><? echo __("Cantidad");?></th>
					</tr>
					<? foreach($event['Type']['Entry'] as $entry):?>
					<? if($isdinner == $entry['isdinner']):?>
						<tr>
							<th class="em-ticket em-bookings-ticket-table-type">
								<b><? echo $entry['title'];?></b><br><span class="ticket-desc"><? echo $entry['description'];?></span>

							</th>
							<th class="em-bookings-ticket-table-price"><? echo $entry['price'];?></th>
							<th class="em-bookings-ticket-table-spaces">
							<input name="data[Item][<? echo $entry['id'];?>][price]" value="<? echo $entry['price'];?>" type="hidden">
								<select name="data[Item][<? echo $entry['id'];?>][num]" class="em-ticket-select" id="em-ticket-spaces-17408">
									<option>0</option>
									<option>1</option>
									<option>2</option>
									<option>3</option>
									<option>4</option>
									<option>5</option>
									<option>6</option>
									<option>7</option>
									<option>8</option>
									<option>9</option>
									<option>10</option>
								</select>

							</th>
						</tr>
					<? endif;?>
					
						
					<? endforeach;?>
				</tbody>

			</table>
			<div class="em-booking-form-details">
				<p class="input-name input-user-field"> 
					<label for="user_name"><? echo __("Nombre y apellidos");?> <span class="em-form-required">*</span> 
					</label> 
					<input type="text" name="data[Order][name]" id="user_name" class="input" value="">
				</p>

				<p class="input-user_email input-user-field"> 
					<label for="user_email"> <? echo __("Correo electrónico");?> <span class="em-form-required">*</span> 
					</label> 
					<input type="text" name="data[Order][email]" id="user_email" class="input" value="">
				</p>

				<p class="input-group input-text input-field-user_email2"> 
					<label for="user_email2"> <? echo __("Repita el correo electrónico");?> </label> 
					<input type="text" value="" class="input" id="user_email2" name="data[Order][emailr]">
				</p>

				<p class="input-group input-text input-field-dbem_phone"> 
					<label for="dbem_phone"> <? echo __("Teléfono");?> <span class="em-form-required">*</span> 
					</label> 
					<input type="text" name="data[Order][phone]" id="dbem_phone" class="input" value="">
				</p>

				<p class="input-group input-checkbox input-field-deseo_recibir_newsletters_er"> 
					<label for="deseo_recibir_newsletters_er"> <? echo __("Si no desea recibir información de nuestras novedades o");?> <a href="http://cardamomo.es/espectaculos-flamenco/"><? echo __("información sobre nuestra programación");?></a>, <? echo __("marque esta casilla.");?> </label> 
					<input type="checkbox" name="data[Order][newsletter]" id="deseo_recibir_newsletters_er" value="1"></p><p class="input-group input-checkbox input-field-dbem_entrada_regalo"> 
					<label for="dbem_entrada_regalo"> <? echo __("Entrada regalo");?> </label> 
					<!-- <input type="checkbox" name="dbem_entrada_regalo" id="dbem_entrada_regalo" value="1"> -->
				</p> 

				

				<div class="em-booking-gateway-form" id="em-booking-gateway-paypal">
					<? echo  $this->Html->image('/theme/Cardamomo/images/paypal_info.png', array('alt'=>'Paypal', 'width' => '228' , 'height' => '61',));
					?> 
					
				</div>

				<div class="em-booking-buttons"> 
					<input type="submit" class="em-booking-submit" value="Reserve este Espectáculo">
				</div>
			</div>

		</form>
	</div>
	<? endif;?>
</div>
<? endif;?>