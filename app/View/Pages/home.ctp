<div class="sequence page-header" style="background-image: url(<? echo $this->Html->url('/');?>theme/Cardamomo/images/bailaora_home.png); background-repeat: no-repeat;background-position: center;">
	

	<div class="NYT">
		<a target="_blank" href="http://goo.gl/Ht3Saf"> 
		
			<? echo  $this->Html->image('/theme/Cardamomo/images/logoNYT_white.png', array('alt'=>'New York Times', 'width' => '300' , 'height' => '100',));
					?> 
			<p><? echo __("El único tablao flamenco en Madrid recomendado por el New York Times. Leer +");?></p>
		</a>
	</div>
	<ul class="mhome">
	
	<? foreach($menuhome as $menu):?>
		<li><a href="<? echo $this->Html->url('/');?><? echo $menu['Page']['slug'];?>"><? echo $menu['Menu']['name'];?></a></li>
		
	<? endforeach;?>
	</ul>

</div>
<div class="page-links page-links-col-3">
		<div class="page-links-box customBG">

				<video  id='video-player' class="video-js vjs-default-skin" controls preload="none" poster="<? echo $this->Html->url('/');?>img/poster.png" data-setup="{}">
				    <source src="<? echo $this->Html->url('/');?>files/flamenco.mp4" type='video/mp4; codecs="avc1.42E01E, mp4a.40.2"'/>
				    <!-- Tracks need an ending tag thanks to IE9 -->
				    <p class="vjs-no-js">To view this video please enable JavaScript, and consider upgrading to a web browser that <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a></p>
				  </video> 
			
			 	
			  
			 
		</div>

		<div class="page-links-box customBG"> 
			<div class="homeespectaculos">
				<h4><? echo __("Espectáculos");?></h4>
				<? $num = 1;?>
				<? foreach ($eventsmonth as $item):?>
					<div class="item_h">
						<img src="<? echo $this->Html->url('/');?>files/type/image/<? echo $item['Type']['image_dir'];?>/home_<? echo $item['Type']['image'];?>" alt="<? echo $item['Type']['name'];?>" width="68px" height="68px">
						<div>
							<h5><? echo $num;?> <? echo __("pase");?>, <? echo $item['Type']['start'];?></h5>
							<p><? echo __("Cuadro flamenco de");?> <? echo $item['Type']['num_artists'];?> <? echo __("artistas");?></p>
							<a href="#"><? echo __("más información");?></a>
							
						</div>
					</div>
					<? $num++;?>
					
				<? endforeach;?>
				
				
			</div>
			
			
		</div>
		<div class="page-links-box customBG"> 
			<ul class="bxslider"></ul>
		</div>
	</div>