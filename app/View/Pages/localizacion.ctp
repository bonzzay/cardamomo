<h1 class="post-title"><? echo $content['Content']['title'];?></h1>
<hr class="thick">
<div class="entry-body">
<? echo $content['Content']['text'];?>
	<? if($content['Content']['slider_id'] != 0){
		echo $this->element('slider', array('sliders' =>$content['Slider'],'id' => 'bxslider'));
	}?>
	<h3><? echo __("¿Desea Contactar con nosotros?");?></h3>
	<p>
		<? echo __("Puede trasladarnos su consulta o comentario cumplimentando el siguiente formulario. De 10:00 a 24:00 horas también estamos a su disposición en los teléfonos ");?><a href="tel:+34918051038">(+34) 918 051 038</a>, <a href="tel:+34691022117">(+34) 691 022 117</a>
	</p>
	<div class="wpcf7" id="wpcf7-f49-p37-o1" lang="en-US" dir="ltr">
		<div class="em-booking-message-error em-booking-message"><? echo $this->Session->flash();?></div>
		<form name="" action="<? echo $this->Html->url('/');?>localizacion-contacto" method="post" class="wpcf7-form" novalidate="novalidate">
			<p><? echo __("Nombre");?>*<br> 
				<span class="wpcf7-form-control-wrap">
					<input type="text" name="data[Contact][name]" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required">
				</span>
			</p>
			<p><? echo __("Email");?>*<br> 
				<span class="wpcf7-form-control-wrap">
					<input type="text" name="data[Contact][email]" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required">
				</span>
			</p>
			<p><? echo __("Mensaje");?>*<br> 
				<span class="wpcf7-form-control-wrap">
					<textarea name="data[Contact][message]" cols="40" rows="10" class="wpcf7-form-control wpcf7-textarea" ></textarea>
				</span>
			</p>


			
			<p>
				<input type="submit" value="Enviar" class="wpcf7-form-control wpcf7-submit">
			</p>
			<p>
				<input type="checkbox" value="1" id="newsLetterCheck" name="data[Contact][newslletr]"><? echo __("Si no desea recibir información de nuestras novedades o");?> <a href="http://cardamomo.es/espectaculos-flamenco/"><? echo __("información sobre nuestra programación");?></a>, <? echo __("marque esta casilla.");?>
			</p>
		</form>
	</div>
</div>