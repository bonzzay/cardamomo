
<div id="content" class="row">
<ul class="breadcrumb">
	<li><a href="<?php echo $this->Html->url('/'); ?>" class="glyphicons home"><i></i> Home</a></li>
	<li class="divider"></li>
	<li><?php echo __('Iconos sociales'); ?></li>
</ul>
<div class="separator bottom"></div>
<!-- // Breadcrumb END -->

<h3><?php echo __('Iconos sociales'); ?></h3>
	<div class="innerLR">
		<div class="widget">
		
			<div class="widget-head">
				<h4 class="heading"><?php echo __('Iconos sociales'); ?></h4>
			</div>
			<div class="widget-timeline">
			<br>
				<ul id="my-list" class="list-timeline">
				<?php foreach ($socials as $social): ?>
				<li id='Social_<?php echo $social['Social']['id']?>'> <span class="ellipsis"><?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $social['Social']['id']), array('class' => 'btn btn-default btn-xs')); ?></span> <span class="glyphicons activity-icon move" style="cursor:move"><i></i></span>  <span>	<?php echo $social['Social']['name']; ?></span> </li>
				<?php endforeach; ?>
				</ul>
				
			</div><!-- /.table-responsive -->
			
		</div><!-- /.index -->
	
	</div><!-- /#page-content .col-sm-9 -->

</div><!-- /#content .row-fluid -->