<div id="menu" class="hidden-sm hidden-print">
		
	<!-- Scrollable menu wrapper with Maximum height -->
	<div class="slim-scroll" data-scroll-height="800px">
	
	<!-- Sidebar Profile -->
	<span class="profile">
		<a class="img" href="#"><? echo  $this->Html->image('/theme/Cakestrap/images/icon.png', array('alt'=>'person', 'width' => '36' , 'height' => '36'));?></a>
		<span>
			<strong>Bienvenido</strong>
			<a href="<? echo $this->Html->url('/');?>admin/users/logout" class="glyphicons right_arrow">Logout <i></i></a>
		</span>
	</span>
	<!-- // Sidebar Profile END -->
	
	
	<!-- // Sidebar Mini Stats END -->
	
	<!-- Regular Size Menu -->
	<ul>
	
		
		
		<!-- Submenu Level 1 (closed) -->
		<li class="hasSubmenu glyphicons right_arrow">
			<a data-toggle="collapse" href="#submenu-1"><i></i><span>Usuarios</span></a>
			<ul class="collapse" id="submenu-1">
				
				<!-- Submenu Level 1 Regular Items -->
				<li><a href="<? echo $this->Html->url('/');?>admin/users"><span>Listado usuarios</span></a></li>
				<li><a href="<? echo $this->Html->url('/');?>admin/users/add"><span>Nuevo usuario</span></a></li>
				<!-- // Submenu Level 1 Regular Items END -->
				
			</ul>
			
		</li>
		<!-- // Submenu Level 1 END -->
		
		<!-- Submenu Level 1 (closed) -->
		<li class="hasSubmenu glyphicons right_arrow">
			<a data-toggle="collapse" href="#submenu-2"><i></i><span>Menus</span></a>
			<ul class="collapse" id="submenu-2">
				
				<!-- Submenu Level 1 Regular Items -->
				<li><a href="<? echo $this->Html->url('/');?>admin/menus/1"><span>Menu top</span></a></li>
				<li><a href="<? echo $this->Html->url('/');?>admin/menus/2"><span>Menus footer</span></a></li>
				<li><a href="<? echo $this->Html->url('/');?>admin/menus/3"><span>Menus subfooter</span></a></li>
				<li><a href="<? echo $this->Html->url('/');?>admin/menus/4"><span>Menus galería</span></a></li>
				<li><a href="<? echo $this->Html->url('/');?>admin/menus/5"><span>Menus home</span></a></li>
				
				
				



				
				<!-- // Submenu Level 1 Regular Items END -->
				
			</ul>
			
		</li>
		<!-- // Submenu Level 1 END -->

		<!-- Submenu Level 1 (closed) -->
		<li class="hasSubmenu glyphicons right_arrow">
			<a data-toggle="collapse" href="#submenu-3"><i></i><span>Contenido</span></a>
			<ul class="collapse" id="submenu-3">
				
				<!-- Submenu Level 1 Regular Items -->
				<li><a href="<? echo $this->Html->url('/');?>admin/contents"><span>Listado páginas</span></a></li>
				<li><a href="<? echo $this->Html->url('/');?>admin/contents/add"><span>Nueva página</span></a></li>
				<li><a href="<? echo $this->Html->url('/');?>admin/artists"><span>Listado de artistas</span></a></li>
				<li><a href="<? echo $this->Html->url('/');?>admin/artists/add"><span>Nuevo artista</span></a></li>
				<!-- // Submenu Level 1 Regular Items END -->
				<li><a href="<? echo $this->Html->url('/');?>admin/socials"><span>Iconos sociales</span></a></li>
				


			</ul>
			
			
		</li>
		<!-- // Submenu Level 1 END -->
		<!-- Submenu Level 1 (closed) -->
		<li class="hasSubmenu glyphicons right_arrow">
			<a data-toggle="collapse" href="#submenu-4"><i></i><span>Multimedia</span></a>
			<ul class="collapse" id="submenu-4">
				
				<!-- Submenu Level 1 Regular Items -->
				<li><a href="<? echo $this->Html->url('/');?>admin/sliders"><span>Sliders</span></a></li>
				<li><a href="<? echo $this->Html->url('/');?>admin/sliders/add"><span>nuevo slider</span></a></li>

				<li><a href="<? echo $this->Html->url('/');?>admin/galeries"><span>Galerías</span></a></li>
				<li><a href="<? echo $this->Html->url('/');?>admin/galeries/add"><span>Nueva galería</span></a></li>
				<!-- // Submenu Level 1 Regular Items END -->
				
			</ul>
			
		</li>

		<!-- Submenu Level 1 (closed) -->
		<li class="hasSubmenu glyphicons right_arrow">
			<a data-toggle="collapse" href="#submenu-5"><i></i><span>Eventos</span></a>
			<ul class="collapse" id="submenu-5">
				
				<!-- Submenu Level 1 Regular Items -->
				<li><a href="<? echo $this->Html->url('/');?>admin/events"><span>Eventos</span></a></li>
				<li><a href="<? echo $this->Html->url('/');?>admin/events/add"><span>Nuevo Evento</span></a></li>
				<li><a href="<? echo $this->Html->url('/');?>admin/events/periodic"><span>Evento periódico</span></a></li>

				<li><a href="<? echo $this->Html->url('/');?>admin/types"><span>Tipos de eventos</span></a></li>
				<li><a href="<? echo $this->Html->url('/');?>admin/types/add"><span>Nuevo Tipo de evento</span></a></li>


				<li><a href="<? echo $this->Html->url('/');?>admin/entries"><span>Tipos de entradas</span></a></li>
				<li><a href="<? echo $this->Html->url('/');?>admin/entries/add"><span>Nuevo Tipo de entradas</span></a></li>
				<!-- // Submenu Level 1 Regular Items END -->
				
			</ul>
			
		</li>
		

						
						
	</ul>
	<div class="clearfix"></div>
	<div class="separator bottom"></div>
	<!-- // Regular Size Menu END -->
	
				
				
				
	</div>
	<!-- // Scrollable Menu wrapper with Maximum Height END -->
	
</div>