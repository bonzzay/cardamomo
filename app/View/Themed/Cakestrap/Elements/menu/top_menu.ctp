<? //if(isset($isAdmin) and $isAdmin != null):?>
<div class="navbar main hidden-print">

<a href="<? echo $this->Html->url('/');?>" class="appbrand pull-left"><span>Cardamomo <span>Admin</span></span></a>

<a href="<?php echo $this->Html->url('/'); ?>admin/contents/?lang=esp" class="btn btn-<? if($idioma == 'esp'){echo 'success';}else{echo 'default';};?>">Español</a>
<a href="<?php echo $this->Html->url('/'); ?>admin/contents/?lang=eng"  class="btn btn-<? if($idioma == 'eng'){echo 'success';}else{echo 'default';};?>">Ingles</a>
<a href="<?php echo $this->Html->url('/'); ?>admin/contents/?lang=fra"  class="btn btn-<? if($idioma == 'fra'){echo 'success';}else{echo 'default';};?>">Frances</a>
<a href="<?php echo $this->Html->url('/'); ?>admin/contents/?lang=por"  class="btn btn-<? if($idioma == 'por'){echo 'success';}else{echo 'default';};?>">Portugues</a>
<a href="<?php echo $this->Html->url('/'); ?>admin/contents/?lang=ger"  class="btn btn-<? if($idioma == 'ger'){echo 'success';}else{echo 'default';};?>">Aleman</a>
<a href="<?php echo $this->Html->url('/'); ?>admin/contents/?lang=chi"  class="btn btn-<? if($idioma == 'chi'){echo 'success';}else{echo 'default';};?>">Chino</a>
<a href="<?php echo $this->Html->url('/'); ?>admin/contents/?lang=ita"  class="btn btn-<? if($idioma == 'ita'){echo 'success';}else{echo 'default';};?>">Italiano</a>
<a href="<?php echo $this->Html->url('/'); ?>admin/contents/?lang=rus"  class="btn btn-<? if($idioma == 'rus'){echo 'success';}else{echo 'default';};?>">Ruso</a>
<a href="<?php echo $this->Html->url('/'); ?>admin/contents/?lang=jpn"  class="btn btn-<? if($idioma == 'jpn'){echo 'success';}else{echo 'default';};?>">Japonés</a>
</div>


<? //endif;?>