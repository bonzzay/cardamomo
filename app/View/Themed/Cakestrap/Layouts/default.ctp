<?php
/**
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       Cake.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

$cakeDescription = __d('cake_dev', 'Cardamomo Admin');
?>
<?php echo $this->Html->docType('html5'); ?> 
<html>
	<head>
		<?php echo $this->Html->charset(); ?>
		<title>
			<?php echo $cakeDescription ?>:
			<?php echo $title_for_layout; ?>
		</title>
		<?php
			echo $this->Html->meta('icon');
			
			echo $this->fetch('meta');

			echo $this->Html->css('bootstrap');
			echo $this->Html->css('bootstrap-select');
			//echo $this->Html->css('main');
			echo $this->Html->css('glyphicons_social');
			echo $this->Html->css('glyphicons_filetypes');
			echo $this->Html->css('glyphicons_regular');
			echo $this->Html->css('font-awesome.min');
			echo $this->Html->css('uniform.default');
			echo $this->Html->css('prettyPhoto');
			echo $this->Html->css('jquery.easy-pie-chart');
			echo $this->Html->css('datetimepicker');
			echo $this->Html->css('select2');
			echo $this->Html->css('bootstrap-switch');
			echo $this->Html->css('main');


			echo $this->Html->script('libs/jquery-1.10.2.min');


			



			


			echo $this->Html->css('style-light');
			

			echo $this->fetch('css');
			
			
			echo $this->Html->script('libs/less.min');

			
			echo $this->fetch('script');
		?>
		<script>
var primaryColor = '#e25f39',
	dangerColor = '#bd362f',
	successColor = '#609450',
	warningColor = '#ab7a4b',
	inverseColor = '#45484d';
</script>

<!-- Themer -->
<script>
var themerPrimaryColor = primaryColor;
</script>


	
	
	</head>

	<body class="">

		<div id="container fluid menu-left">
		
			
				<?php echo $this->element('menu/top_menu'); ?>
				<div id="wrapper">
				<?php echo $this->element('menu/menu'); ?>
					<?php echo $this->Session->flash(); ?>
					<?php echo $this->fetch('content'); ?>
					<small>
						<?php echo $this->element('sql_dump'); ?>
					</small>

				</div>
				
		
		
			
		</div><!-- /#main-container -->
		
		<?php echo $this->element('menu/footer'); ?>
		
		
	</body>


	<?php
		

		echo $this->Html->script('libs/jquery-ui.min');
		echo $this->Html->script('libs/jquery.ui.touch-punch.min');
		echo $this->Html->script('libs/modernizr');
		echo $this->Html->script('libs/bootstrap.min');
		echo $this->Html->script('libs/bootstrap-select');
		echo $this->Html->script('libs/jquery.slimscroll.min');
		echo $this->Html->script('libs/common');
		echo $this->Html->script('libs/holder');
		echo $this->Html->script('libs/jquery.uniform.min');
		echo $this->Html->script('libs/jquery.prettyPhoto');
		echo $this->Html->script('libs/bootstrap-switch.min');
		echo $this->Html->script('libs/select2');
		echo $this->Html->script('libs/jquery.easypiechart');
		echo $this->Html->script('libs/bootstrap-datetimepicker.min');
		echo $this->Html->script('libs/jquery.sparkline.min');
		echo $this->Html->script('libs/jquery.multi-select');
		echo $this->Html->script('libs/jquery.flot');
		echo $this->Html->script('libs/jquery.flot.pie');
		






		


	?>


<script type="text/javascript">
	$(window).load(function(){ 
		//link add
		$('.addlinka').click(function(e) {
			e.preventDefault();
			var url = $(this).attr('href');
			$('#appendedInputButtons').val(url);
			$('#linkModal').modal('hide')


		});

		//link sortable
		$("#my-list").sortable({stop:function (event, ui) {$.post("/socials/reorder", $("#my-list").sortable("serialize"))}});
		$("#my-list-menu").sortable({stop:function (event, ui) {$.post("/menus/reorder", $("#my-list-menu").sortable("serialize"))}});

		$("#my-list-artist").sortable({stop:function (event, ui) {$.post("/artists/reorder", $("#my-list-artist").sortable("serialize"))}});



		$("#datetimepicker1").datetimepicker({
				format: 'yyyy-mm-dd hh:ii',
				startDate: "2015-02-14 10:00"
			});
		$("#datetimepicker2").datetimepicker({
				format: 'yyyy-mm-dd hh:ii',
				startDate: "2015-02-14 10:00"
			});
		$("#datetimepicker11").datetimepicker({
			//viewMode: 'years',
			startDate: "2015-02-01",

            format: "yyyy-mm-dd",
		    startView: "year", 
    minView: "year"
		});


	


		$('#optgroup').multiSelect({ selectableOptgroup: true});
		$('#optgroup2').multiSelect({ selectableOptgroup: true});
		$('#optgroup3').multiSelect({ selectableOptgroup: true});
		$('#optgroup4').multiSelect({ selectableOptgroup: true});
		
		var newatt = $('#editinfo90').attr('data-val');

	    if (newatt) {
	       	var array = newatt.split(",");
	       	
	        $('#optgroupedit').multiSelect({ selectableOptgroup: true});
	        $('#optgroupedit').multiSelect('select', array);
	    }
		
		var idimage = 0;
		var idmesures = 0;

		$('#addmorepli').click(function(e) {
			e.preventDefault();
			idmesures= idmesures+1;
			
			$('#contentrepli').append('<div class="form-group col-md-6" ><label>M2</label><input name="data[Measures]['+idmesures+'][Measure][m2]" class="form-control" ></div><div class="form-group col-md-6" ><label>Euro /m2</label><input name="data[Measures]['+idmesures+'][Measure][eurom2]" class="form-control"></div>');
		});
		
		$('#addmogallery').click(function(e) {
			e.preventDefault();
			idimage= idimage+1;
			$('#contentgallery').append('<div class="form-group col-md-4"><label>Nombre</label><input name="data[Images]['+idimage+'][Image][name]" class="form-control" ></div><div class="form-group col-md-4" ><label>Imagen</label><input name="data[Images]['+idimage+'][Image][image]" class="form-control" type="file"></div><div class="form-group col-md-4" ><label>Orden</label><input name="data[Images]['+idimage+'][Image][order]" class="form-control"></div>');
		});
		$('.deletemeasure').on('click', function(e) {
	      e.preventDefault();
	      var valu = $(this).attr('data-val');
	      
	      $.ajax({
	        type    : "POST",
	        data  : { Measure: { id: valu} },
	        cache : false,
	        dataType: 'json',
	        url   : "/admin/products/delete_measure",
	        success: function(data) {
	           $('#measuredelete'+valu).hide();
	           
	        }
	      });
	    });
	    $('.deleteimage').on('click', function(e) {
	      e.preventDefault();
	      var valu = $(this).attr('data-val');
	      console.log(valu);
	      $.ajax({
	        type    : "POST",
	        data  : { Image: { id: valu} },
	        cache : false,
	        dataType: 'json',
	        url   : "/admin/products/delete_image",
	        success: function(data) {
	           $('#imagedelete'+valu).hide();
	           
	        }
	      });
	    });


	    $('#optgroup').multiSelect({ selectableOptgroup: true});

	});



	
</script>


	

</html>