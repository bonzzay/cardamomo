<!-- BEGIN .site-footer-wrapper -->

<footer class="site-footer-wrapper">

	<!-- BEGIN .site-footer -->
	<div class="site-footer">
		
		<!-- BEGIN .site-width -->
		<div class="site-width">
			
			<!-- BEGIN .footer-content -->
			<div class="footer-content">
				
				
				<!-- logo -->
				<a class="footer-logo" href="<? echo $this->Html->url('/');?>" title="Cardamomo "> 
				<? echo  $this->Html->image('/theme/Cardamomo/images/footerlogo.png', array('alt'=>'logo', 'width' => '164' , 'height' => '38'));?>
				</a>
				
			
				<!-- BEGIN .footer-contact-info -->
				<div class="footer-contact-info">
						
					<!-- Title -->
					<h4 itemprop="name"><? echo __("Cardamomo");?></h4>
						
						<div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">					
						
							<div class="half">
								
								<span class="address"><? echo __("Echegaray 15 - Madrid - España");?></span>
							
							</div>								
							
							<div class="half end">
								<span class="telephone" itemprop="telephone"><? echo __("+34 918 051 038")?></span>
								<span class="email" itemprop="email"><a href="mailto:contacto@cardamomo.es">contacto@cardamomo.es</a></span>
								
							</div>
						
						</div>
						<div class="tripAdvisorHome">
							<a target="_blank" href="http://www.tripadvisor.es/Attraction_Review-g187514-d244580-Reviews-Cardamomo_Tablao_Flamenco-Madrid.html">
							
							<? echo  $this->Html->image('/theme/Cardamomo/images/trip.png', array('alt'=>'Certificado TripAdvisor', 'width' => '127' , 'height' => '120'));?>
							</a>
						</div>
						<div class="tripAdvisorHome certificate2015">
							<a target="_blank" href="http://www.tripadvisor.es/Attraction_Review-g187514-d244580-Reviews-Cardamomo_Tablao_Flamenco-Madrid.html">
							
								<? echo  $this->Html->image('/theme/Cardamomo/images/2015trip.jpg', array('alt'=>'Certificado TripAdvisor', 'width' => '114' , 'height' => '120'));?>
							</a>
						</div>
						
					</div>
					
				
				<!-- BEGIN .footer-navigation -->
				<div class="footer-navigation">
					
					<h4><? echo __("Links");?></h4>
					<ul id="footer-links" class="clearfix">
					<? foreach($topfooter as $menu):?>
						<li id="menu-item-<? echo $menu['Menu']['id'];?>" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-<? echo $menu['Menu']['id'];?>"><a href="<? echo $this->Html->url('/');?><? echo $menu['Page']['slug'];?>"><? echo $menu['Menu']['name'];?></a></li>

					<? endforeach;?>


						
					</ul>
					
					
					
					<!-- Social -->
					<h4><? echo __("Síguenos en");?></h4>
						<? foreach($socials as $social):?>
							<? 
							if($social['Social']['id'] == 1){
								$class="facebook";
							}else if($social['Social']['id'] == 2){
								$class="twitter";
							}
							else if($social['Social']['id'] == 3){
								$class="googleplus";
							}
							else if($social['Social']['id'] == 4){
								$class="youtube";
							}
							else if($social['Social']['id'] == 5){
								$class="flickr";
							}
							else if($social['Social']['id'] == 6){
								$class="tripadvisor";
							}
							else if($social['Social']['id'] == 7){
								$class="blog";
							}

							;?>
							<a class="social-button <? echo $class;?>" href="<? echo $social['Social']['link'];?>" title="<? echo $social['Social']['title'];?>" target="_blank"><span class="funky-icon-<? echo $class;?>"></span></a>
						<? endforeach;?>

						

						
						
						
					
				</div>
				<!-- END .footer-navigation -->
				
			</div>
			<!-- END .footer-content -->			
			
			<!-- BEING .copyright -->
			<div class="copyright">
				
				<div class="copyright-content">
					
					&copy; <?php echo date( "Y" ); ?> <a href="#">Cardamomo</a>
					<? foreach($subfooter as $menu):?>
						
						<a href="<? echo $this->Html->url('/');?><? echo $menu['Page']['slug'];?>"><? echo $menu['Menu']['name'];?></a>

					<? endforeach;?>
					
					
					
					
				</div>

				
				
			</div>
			<!-- END .copyright -->			
		
		</div>
		<!-- END .site-width -->
		
	</div>
	<!-- END .site-footer -->

	
</footer>
<!-- END .site-footer-wrapper -->
