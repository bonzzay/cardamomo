<head>
	<link rel="profile" href="http://gmpg.org/xfn/11" />
	<META name="ROBOTS" content="NOINDEX, NOFOLLOW">
	<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
	<meta name="format-detection" content="telephone=no">

	<meta name="description" content="<?php echo $description_for_layout; ?>" />
    <meta name="keywords" content="<?php echo $keywords_for_layout; ?>" />
    <meta name="author" content="Cardamomo Tablo flamenco" />
    <meta name="dcterms.rightsHolder" content="Cardamomo Tablo flamenco" />
    <link rel="stylesheet" id="redux-google-fonts-css" href="http://fonts.googleapis.com/css?family=Raleway%3A400%7CMontserrat%3A700%2C400&amp;ver=4.0.8" type="text/css" media="all">
	<title>
            <?php echo $title_for_layout; ?>
    </title>




	<?php
		echo $this->Html->charset();
		echo $this->Html->meta('icon');
		echo $this->fetch('meta');
		// css
		
		echo $this->Html->css('style');
		echo $this->Html->css('global');
		echo $this->Html->css('responsive');
		echo $this->Html->css('ie');
		echo $this->Html->css('jquery.bxslider');
		echo $this->Html->css('bootstrap-datetimepicker');
		echo $this->Html->css('video-js.min');
			
	?>


</head>