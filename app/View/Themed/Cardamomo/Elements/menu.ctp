<header id="header" class="site-header">
	<div class="site-width clearfix"> 
		<a class="header-logo valign-middle-holder" href="<? echo $this->Html->url('/');?>" title="Cardamomo " itemprop="url"> 
		
		<? echo  $this->Html->image('/theme/Cardamomo/images/logocardamomo.png', array('alt'=>'cardamomo-tablao-flamenco', 'title'=>'cardamomo-tablao-flamenco','width' => '392' , 'height' => '120', 'class'=>'image-logo'));?> 
		</a>
		<div class="info">
			<p> <a href="http://maps.google.com/?q=Echegaray%2015%20-%20Madrid%20-%20España" title="Mapa-Flamenco-Madrid">C/ ECHEGARAY 15 • MADRID / METRO: SOL - SEVILLA</a></p>
			<p class="phones">(+34) 918 051 038 <span style="display: inline-block; width: 10px;"></span></p>
			<p class="contact">
				<a alt="<? echo __("email-Cardamomo-Madrid");?>" title="<? echo __("email-Cardamomo-Madrid");?>" href="mailto:contacto@cardamomo.es">
				<? echo  $this->Html->image('/theme/Cardamomo/images/contact.png', array('alt'=>'Mail', 'width' => '20' , 'height' => '15'));?> 
				
				</a>
				<? echo  $this->Html->image('/theme/Cardamomo/images/wifiFree.png', array('alt'=>'free wifi', 'width' => '55' , 'height' => '30', 'class' => 'wifiIcon'));?>

			</p>
			
		</div>

		<div class="socialMedia_Header">
			<? foreach($socials as $social):?>
				<? 
				if($social['Social']['id'] == 1){
					$class="facebook";
				}else if($social['Social']['id'] == 2){
					$class="twitter";
				}
				else if($social['Social']['id'] == 3){
					$class="googleplus";
				}
				else if($social['Social']['id'] == 4){
					$class="youtube";
				}
				else if($social['Social']['id'] == 5){
					$class="flickr";
				}
				else if($social['Social']['id'] == 6){
					$class="tripadvisor";
				}
				else if($social['Social']['id'] == 7){
					$class="blog";
				}

				;?>
				<a class="social-button <? echo $class;?>" href="<? echo $social['Social']['link'];?>" title="<? echo $social['Social']['title'];?>" target="_blank"><span class="funky-icon-<? echo $class;?>"></span></a>
			<? endforeach;?>

			

		</div>
<? $url = explode("?", $_SERVER['REQUEST_URI']);?>
<? $http = "http://$_SERVER[HTTP_HOST]$url[0]";?>
		<ul class="headerLanguage">
			<li id="spanish">
			<a href="<? echo $url[0];?>?lang=esp">Español</a>
			</li>
			<li id="english"><a href="<? echo $http;?>?lang=eng">Ingles</a></li>
			<li id="french"><a href="<? echo $http;?>?lang=fra">Frances</a></li>
			<li id="portuguese"><a href="<? echo $http;?>?lang=por">Portugues</a></li>
			<li id="german">	<a href="<? echo $http;?>?lang=ger">Alemán</a></li>
			<li id="chinesse">	<a href="<? echo $http;?>?lang=chi">Chino</a></li>
			<li id="italian">	<a href="<? echo $http;?>?lang=ita">Italiano</a></li>
			<li id="russian">	<a href="<? echo $http;?>?lang=rus">Ruso</a></li>
			<li id="japanesse">	<a href="<? echo $http;?>?lang=jpn">Japonés</a></li></ul>

		<div class="header-contact-info valign-middle-holder"><div class="valign-middle"> <span class="telephone">+34 918 051 038</span> <span class="address"> Echegaray 15 - Madrid - España </span><span class="espectaculos address">ESPECTÁCULOS DE LUNES A DOMINGO,<br> A 19:00 y 22:00 H</span></div> <a class="map-link" href="http://maps.google.com/?q=Echegaray%2015%20-%20Madrid%20-%20España" target="_blank" title="Mapa-Flamenco-Madrid">Get Directions</a></div>


		<span class="toggle toggle-menu"><span class="funky-icon-menu"></span></span>

		<span class="toggle toggle-address"> <a href="http://maps.google.com/?q=Echegaray%2015%20-%20Madrid%20-%20España" target="_blank"> <span class="funky-icon-address"></span> </a> </span>

		<nav class="header-navigation" role="navigation">
			<h1 class="hidden">Primary Menu</h1>
			<ul id="primary-nav" class="clearfix">
			<? foreach($topmenus as $menu):?>
				<li id="menu-item-<? echo $menu['Menu']['id'];?>" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-<? echo $menu['Menu']['id'];?>"><a href="<? echo $this->Html->url('/');?><? echo $menu['Page']['slug'];?>"><? echo $menu['Menu']['name'];?></a></li>

			<? endforeach;?>
				<!-- <li id="menu-item-" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-"><a href="<? echo $this->Html->url('/');?>images"><? echo __("Galería");?></a></li>
				<li id="menu-item-" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-"><a href="<? echo $this->Html->url('/');?>artists"><? echo __("Artistas");?></a></li>
				<li id="menu-item-" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-"><a href="<? echo $this->Html->url('/');?>calendar"><? echo __("Calendario");?></a></li> -->
			
				


			</ul> 
			<div id="reserva-online">
				<a href="#"><? echo __("Reserva Entradas");?></a>
			</div>


		</nav>
		<div id="calendario-reserva">
			<form method="post" action="<? echo $this->Html->url('/');?>event">
			<input type="hidden"  id="datetimepicker12" name="data[Cal][date]">
			
			<div >
				<h4><? echo __("Cena+Espectáculos");?></h4>
				<div id="cenamoreespectaculo"> 
				
  				</div>
			</div>
			<div>
				<h4><? echo __("Espectáculos");?></h4>
				<div id="moreespectaculo"> 
				
  				</div>
			</div>
			<div>
				 <input type="submit" value="<? echo __("Siguiente");?>">
			</div>
			</form>
			
		</div>
	

	</div>
</header>