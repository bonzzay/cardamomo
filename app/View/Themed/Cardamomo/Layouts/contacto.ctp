<?php
/**
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       Cake.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

$cakeDescription = __d('cake_dev', 'Clickprinting');
?>
<?php echo $this->Html->docType('html5'); ?> 
<html>
<?php echo $this->element('header'); ?>
	<body itemscope itemtype="http://schema.org/FoodEstablishment" class="home page page-template navigation-rows-2">
		<a id="top"></a>
	
		<!-- Preloader  -->
		<div id="preloader">		
			<div class="site-width">
				<div id="percent-bar"></div>	
			</div>		
		</div>
	
		<!-- BEGIN .wrapper -->
		<div id="wrapper" class="wrapper site-width">		
	

			<?php echo $this->element('menu'); ?>
			
			<!-- BEGIN .page-content -->
			
			<section class="page-content clearfix">
				<div class="google-map half"><iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3037.7019085788497!2d-3.6995289999999996!3d40.415454000000004!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xd42288108cfcdc5%3A0x5b960689e6c07c13!2sCardamomo+Tablao+Flamenco!5e0!3m2!1ses!2s!4v1421940682453" width="100%" height="400px" frameborder="0" style="border:0"></iframe></div>
				<!-- BEGIN .content-padding -->
				<div class="post-37 page type-page status-publish hentry content-padding clearfix sidebar-25">
					<!-- BEGIN .entry-body -->
					
	
					<?php echo $this->fetch('content'); ?>
					
				<!-- END .page-content-->
				</div>
			
				
				<?php echo $this->element('side'); ?>
			</section>	
			
			
		</div>
		<?php echo $this->element('footer'); ?>
	</body>
	<?php

	// scripts
		echo $this->Html->script('vendor/moment.min');
		echo $this->Html->script('vendor/jquery-1.11.2.min');
		
		echo $this->Html->script('vendor/modernizr.min');
		echo $this->Html->script('vendor/jquery.sequence-min');
		echo $this->Html->script('vendor/jquery.stellar.min');
		echo $this->Html->script('vendor/smoothscroll');
		
		echo $this->Html->script('vendor/preloader');
		echo $this->Html->script('vendor/jquery.bxslider.min');
		echo $this->Html->script('vendor/jquery.custom.min');
		
		/*echo $this->Html->script('vendor/fullcalendar.min');
		
		echo $this->Html->script('vendor/lang-all');*/
		echo $this->Html->script('vendor/bootstrap-datetimepicker');
		echo $this->Html->script('vendor/moment');

		
		echo $this->Html->script('vendor/jquery.qtip-1.0.0-rc3.min');

		echo $this->Html->script('vendor/underscore');
		echo $this->Html->script('vendor/backbone');
		echo $this->Html->script('routes');

		
		/*echo $this->Html->script('vendor/underscore');
		echo $this->Html->script('vendor/backbone');
		echo $this->Html->script('routes');*/

		
	
		
	?>
	<script type="text/javascript" src="/theme/Cardamomo/js/custom.js?1.0.0"></script>
	<?php echo $this->Session->flash(); ?>
	
			
</html>