window.mainAll = function () {

	function lang(){
		//lang home
		$(".headerLanguage li").click(function() {

		
			var url = $( this ).find('a').attr('href');
			window.location.href = url;
			
		});

	}
	function calendar(){
		$('#calendario-reserva').hide();

		$('#reserva-online').click(function() {
			$('#calendario-reserva').toggle();
		});
		var dateToday = new Date();
		$('#datetimepicker12').datetimepicker({
                inline: true,
                sideBySide: false,
                format: 'DD/MM/YYYY',
                locale: moment.locale('es'),
                minDate: dateToday,
        });

        $('#datetimepicker12').on('change dp.change', function(e){
		    // do stuff...
		    llamadaAjaxEvent();

		})


	}

	

	

	function llamadaAjaxEvent(){
		var valor = $('#datetimepicker12').val();
			console.log(valor);
			$.ajax({
			 	type  : "POST",
		      	dataType: 'json',
		      	cache : false,
		      	url   : "/jsonevents",
				data  : { Event: { id: valor } },
				error: function(data) {
					console.log('error');
					console.log(data);
				},
				success: function(data) {
					// rellenar Cenas
					console.log(data);
					var eventos = data.events;
					$('#moreespectaculo').html('');
					$.each(eventos, function(i, item) {
					console.log(item);
					    var hora = eventos[i].Entry.start.slice(0,-3)
					    $('#moreespectaculo').append('<input type="radio" name="data[Cal][dateevent]" value="'+hora+'" checked>'+hora);

					   
					});
					// rellenar Eventos
					
					var eventos = data.eventsdinner;
					$('#cenamoreespectaculo').html('');
					$.each(eventos, function(i, item) {
					
					    var hora = eventos[i].Entry.start.slice(0,-3)
					    $('#cenamoreespectaculo').append('<input type="radio" name="data[Cal][dateevent]" value="'+hora+'" checked>'+hora);

					   
					});
				},
				
			});
	}
	
	calendar();
	lang();
	llamadaAjaxEvent();

};

window.contacto = function () {

	function form(){
		
		var errorhtml = $('.em-booking-message-error').html();
		if(errorhtml == ''){
			$('.em-booking-message-error').hide();
		}
	}
	form();
	
}

window.portada = function () {
	function flickr(){
		//assign your api key equal to a variable
			var apiKey = 'ed98e4cc282c905057b7b3cd2c4b67e1';
			var secret ="ca88e6faa778dea1";
			//var apiKey = 'e52aa84644f3750abed22b4d65b8c980';
			var userId = '82841124@N02';
			var photoset_id="72157636208422915";
			$.getJSON('https://api.flickr.com/services/rest/?format=json&method=flickr.photosets.getPhotos&photoset_id=' + photoset_id + '&per_page=1000' + '&page=1' + '&api_key=' + apiKey + '&user_id=' + userId + '&jsoncallback=?', function(data) {
			    $.each(data.photoset.photo, function(i, flickrPhoto){
			        var basePhotoURL = 'http://farm' + flickrPhoto.farm + '.static.flickr.com/'
			        + flickrPhoto.server + '/' + flickrPhoto.id + '_' + flickrPhoto.secret + ".jpg";            
			        var altimg = 'cardamomo photo';
			        var a_href = "http://www.flickr.com/photos/" + data.photoset.owner + "/" + flickrPhoto.id + "/";
			        $("<img />").attr(
			        	{
						 
						  src: basePhotoURL,
						  alt: altimg,

						}).appendTo(".bxslider").wrap(("<li  data-thumb="+basePhotoURL+"></li>"))
				});
			    	   /* $('.bxslider').bxSlider({
						  mode: 'fade',
						  adaptiveHeight: true,
						  captions: false
						});*/
				 
				$(".bxslider").lightSlider({
					auto:true,
				
					pager: false,
					mode: 'fade',
					loop: true,
					item:1,
					thumbItem:0,
					slideMargin: 0,
					speed:500,
					autoWidth: true,
					vertical:false,
					controls:false,
				}); 
			});
		 
	}

	flickr();
	
}

window.calendar = function () {

	/*function ical(){


	    // page is now ready, initialize the calendar...
	    $('#calendar').fullCalendar({
			
			header: {
	    		left:   'title',
	    		center: '',
	    		right:  'prev,next'
			},
			lang: idioma,
			defaultView: 'month',
			firstHour: 8,
			editable: false,
			events: plgFcRoot,
	    	dayClick: function(date, jsEvent, view, resourceObj) {
	    		var today = new Date();
				var dd = today.getDate();
				var mm = today.getMonth()+1; //January is 0!
				var yyyy = today.getFullYear();
				var datefin = yyyy+'-'+mm+'-'+dd;

	    		if (date.format() >= datefin){
		        	window.location.href = document.location.origin+"/day/"+date.format(); 
		        }else{
		        	
		        }
		        

		    }
	    })
		
	}
	ical();*/
}

window.gallery = function () {
	
	function flickr(){
		//assign your api key equal to a variable
			var apiKey = 'ed98e4cc282c905057b7b3cd2c4b67e1';
			var secret ="ca88e6faa778dea1";
			//var apiKey = 'e52aa84644f3750abed22b4d65b8c980';
			var userId = '82841124@N02';
			var photoset_id="72157636208422915";
			$.getJSON('https://api.flickr.com/services/rest/?format=json&method=flickr.photosets.getPhotos&photoset_id=' + photoset_id + '&per_page=1000' + '&page=1' + '&api_key=' + apiKey + '&user_id=' + userId + '&jsoncallback=?', function(data) {
			    $.each(data.photoset.photo, function(i, flickrPhoto){
			        var basePhotoURL = 'http://farm' + flickrPhoto.farm + '.static.flickr.com/'
			        + flickrPhoto.server + '/' + flickrPhoto.id + '_' + flickrPhoto.secret + ".jpg";            
			        var altimg = 'cardamomo photo';
			        var a_href = "http://www.flickr.com/photos/" + data.photoset.owner + "/" + flickrPhoto.id + "/";
			        $("<img />").attr(
			        	{
						 
						  src: basePhotoURL,
						  alt: altimg,

						}).appendTo(".bxslider").wrap(("<li  data-thumb="+basePhotoURL+"></li>"))
				});
			    	   /* $('.bxslider').bxSlider({
						  mode: 'fade',
						  adaptiveHeight: true,
						  captions: false
						});*/
				 
				
			});
		$(".slider").lightSlider({
			  	
	        gallery:true,
            item:1,
            thumbItem:5,
            slideMargin: 5,
            speed:500,
            
            vertical:false,
		}); 
	}

	function video(){
		var video = $("#video-player");
		var windowObj = $(window);
 
		function onResizeWindow() {
			resizeVideo(video[0]);
		}
 
		function onLoadMetaData(e) {
			resizeVideo(e.target);
		}
 
		function resizeVideo(videoObject) {
			var percentWidth = videoObject.clientWidth * 100 / videoObject.videoWidth;
			var videoHeight = videoObject.videoHeight * percentWidth / 100;
			video.height(videoHeight);
		}
 
		video.on("loadedmetadata", onLoadMetaData);
		windowObj.resize(onResizeWindow);
	}
	video();
	flickr();
}



