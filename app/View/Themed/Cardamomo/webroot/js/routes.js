window.BabylonRouter = Backbone.Router.extend({

  initialize: function () {
    this.all();
  },

  routes: {
    "": "home",
    "galeria-de-fotos": "gallery",
    "calendar/:action": "calendar",
    "localizacion-contacto": "contacto",


  },

  all: function () {
    mainAll();
  },
  home: function () {
    portada();
  },
  calendar: function () {
    calendar();
  },
   contacto: function () {
    contacto();
  },
  gallery: function () {
    gallery();
  }

  

  

});

window.navigateTo = function (route) {
  Router.navigate(route, { trigger: true, replace: true });
};

$(function () {
  window.Router = new BabylonRouter();
  Backbone.history.start({ silent: true });
  Backbone.history.loadUrl(window.location.pathname.replace(/^\//, ''));
});
