/*
 * webroot/js/ready.js 
 * CakePHP Full Calendar Plugin
 *
 * Copyright (c) 2010 Silas Montgomery
 * http://silasmontgomery.com
 *
 * Licensed under MIT
 * http://www.opensource.org/licenses/mit-license.php
 */

// JavaScript Document
$(document).ready(function() {
	
	var errorhtml = $('.em-booking-message-error').html();
	if(errorhtml == ''){
		$('.em-booking-message-error').hide();
	}

    // page is now ready, initialize the calendar...
    $('#calendar').fullCalendar({
		
		header: {
    		left:   'title',
    		center: '',
    		right:  'prev,next'
		},
		lang: idioma,
		defaultView: 'month',
		firstHour: 8,
		editable: false,
		events: plgFcRoot,
    	dayClick: function(date, jsEvent, view, resourceObj) {
    		var today = new Date();
			var dd = today.getDate();
			var mm = today.getMonth()+1; //January is 0!
			var yyyy = today.getFullYear();
			var datefin = yyyy+'-'+mm+'-'+dd;

    		if (date.format() >= datefin){
	        	window.location.href = document.location.origin+"/day/"+date.format(); 
	        }else{
	        	
	        }
	        

	    }
    })
	
});
