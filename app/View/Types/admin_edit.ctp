<div id="content" class="row">
	<ul class="breadcrumb">
		<li><a href="<?php echo $this->Html->url('/'); ?>" class="glyphicons home"><i></i> Home</a></li>
		<li class="divider"></li>
		<li><?php echo __('Editar tipo de evento'); ?></li>
	</ul>
	<div class="separator bottom"></div>
<!-- // Breadcrumb END -->

	<h3><?php echo __('Editar tipo de evento'); ?></h3>

	
		<div class="innerLR">

		<?php echo $this->Form->create('Type', array('role' => 'form', 'class' => 'form-horizontal', 'enctype'=>'multipart/form-data')); ?>
	
		<!--form class="form-horizontal" style="margin-bottom: 0;" id="validateSubmitForm" method="get" autocomplete="off" novalidate="novalidate"-->
		
		<!-- Widget -->
			<div class="widget">
			
				<!-- Widget heading -->
				<div class="widget-head">
					<h4 class="heading"><?php echo __('Editar tipo de evento'); ?></h4>
				</div>
								<!-- // Widget heading END -->
				
								<div class="widget-body row">
									<div class="col-md-12">
															<div class="form-group">
						<?php echo $this->Form->input('id', array('class' => 'form-control')); ?>
					</div><!-- .form-group -->
					<div class="form-group">
						<?php echo $this->Form->input('name', array('class' => 'form-control','label' => 'Nombre')); ?>
					</div><!-- .form-group -->
					
					<div class="form-group">
						<?php echo $this->Form->input('start', array('class' => 'form-control col-md-3','label' => 'Empieza')); ?>
					</div><!-- .form-group -->
					<div class="form-group">
						<?php echo $this->Form->input('num_artists', array('class' => 'form-control','label' => 'Nº artistas')); ?>
					</div><!-- .form-group -->
					<div class="form-group">
						<?php echo $this->Form->input('price', array('class' => 'form-control','label' => 'Precio')); ?>
					</div><!-- .form-group -->
					<div class="row">
						<div class="form-group col-md-4">
							<?php echo $this->Form->input('Artist', array('label' => 'Cantaores', 'id'=> 'optgroup'));?>
						</div><!-- .form-group -->
						<div class="form-group col-md-4">
								<?php echo $this->Form->input('Guitar', array('label' => 'Guitarristas', 'id'=> 'optgroup3'));?>
						</div><!-- .form-group -->
						<div class="form-group col-md-4">
								<?php echo $this->Form->input('Dance', array('label' => 'Bailaores', 'id'=> 'optgroup4'));?>
						</div><!-- .form-group -->
						
					</div>
					<div class="form-group">
							<?php echo $this->Form->input('Entry',array('label' => 'Entrada', 'id'=> 'optgroup2'));?>
					</div><!-- .form-group -->
					<div class="form-group">
						<label>Mes</label>
						
						<input size="16" type="text" class="form-control" name="data[Type][month]" value="<? echo $this->request->data['Type']['month'];?>"  id="datetimepicker11"/>
						    
					    
					</div>
					<div class="form-group">
						<?php echo $this->Form->input('image', array('class' => 'form-control','type' => 'file', 'label' => 'Imagen pequeña listado')); ?>
					</div><!-- .form-group -->
					

									</div>
										
									<!-- // Row END -->
									
										
								</div>
								<!-- // Row END -->
				
					<hr class="separator">
					
					<!-- Form actions -->
					<div class="form-actions">

						<button type="submit" class="btn btn-icon btn-primary glyphicons circle_ok"><i></i>Guardar</button>
						
					</div>
					<!-- // Form actions END -->
					
			</div>
		</div>
		<!-- // Widget END -->
		
				<?php echo $this->Form->end(); ?>
			
	</div><!-- /#page-content .col-sm-9 -->