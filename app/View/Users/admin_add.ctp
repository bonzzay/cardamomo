<div id="content" class="row">
	<ul class="breadcrumb">
		<li><a href="<?php echo $this->Html->url('/'); ?>" class="glyphicons home"><i></i> Home</a></li>
		<li class="divider"></li>
		<li><?php echo __('Nuevo usuario'); ?></li>
	</ul>
	<div class="separator bottom"></div>
<!-- // Breadcrumb END -->

	<h3><?php echo __('Nuevo usuario'); ?></h3>

	
		<div class="innerLR">

		<?php echo $this->Form->create('User', array('role' => 'form', 'class' => 'form-horizontal')); ?>
	
		<!--form class="form-horizontal" style="margin-bottom: 0;" id="validateSubmitForm" method="get" autocomplete="off" novalidate="novalidate"-->
		
		<!-- Widget -->
			<div class="widget">
			
				<!-- Widget heading -->
				<div class="widget-head">
					<h4 class="heading"><?php echo __('Nuevo usuario'); ?></h4>
				</div>
								<!-- // Widget heading END -->
				
								<div class="widget-body row">
									<div class="col-md-12">
															<div class="form-group">
						<?php echo $this->Form->input('type', array('class' => 'selectpicker col-md-12', 'options' => $options, 'label' => 'Tipo')); ?>
					</div><!-- .form-group -->
					<div class="form-group">
						<?php echo $this->Form->input('email', array('class' => 'form-control', 'label' => 'Email')); ?>
					</div><!-- .form-group -->
					<div class="form-group">
						<?php echo $this->Form->input('password', array('class' => 'form-control', 'label' => 'Contraseña')); ?>
					</div><!-- .form-group -->
					<div class="form-group">
						<?php echo $this->Form->input('rpassword', array('class' => 'form-control', 'label' => 'Repita la contraseña')); ?>
					</div><!-- .form-group -->

									</div>
										
									<!-- // Row END -->
									
										
								</div>
								<!-- // Row END -->
				
					<hr class="separator">
					
					<!-- Form actions -->
					<div class="form-actions">

						<button type="submit" class="btn btn-icon btn-primary glyphicons circle_ok"><i></i>Save</button>
						
					</div>
					<!-- // Form actions END -->
					
			</div>
		</div>
		<!-- // Widget END -->
		
				<?php echo $this->Form->end(); ?>
			
	</div><!-- /#page-content .col-sm-9 -->