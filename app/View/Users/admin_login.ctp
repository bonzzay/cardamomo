<div id="login">

	<!-- Box -->
	<div class="form-signin">
		<h3>Login Admin</h3>
		
		<!-- Row -->
		<div class="row innerLR ">
			
				<!-- Column -->
				<div class="col-md-7 border-right innerT innerB">
					<div class="innerAll">
					
						<!-- Form -->
						<?php echo $this->Form->create('User',array('action'=>'login'));?>
						
							<?php
								echo $this->Form->input('email', array('label'=> false, 'class'=>'form-control','placeholder' => __('email')));
								echo $this->Form->input('password', array('label'=> false, 'class'=>'form-control','placeholder' => __('contraseña')));
							?>
							<div class="row">
								<div class="col-md-5 center">
									<button class="btn btn-block btn-primary" type="submit">Ingresar</button>
								</div>
								
							</div>
						</form>
						<!-- // Form END -->
						</div>
					
				</div>
				<!-- // Column END -->
				
				


			
		</div>
		<!-- // Row END -->
		
		<div class="ribbon-wrapper"><div class="ribbon primary">Admin</div></div>
	</div>
	<!-- // Box END -->
	
</div>